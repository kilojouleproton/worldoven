#!/usr/bin/env python
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import numpy as np
from astropy.convolution import convolve, kernels
import bluenoise
from scipy.spatial import Voronoi, voronoi_plot_2d
import time

class Provincegon(Polygon): 
	provid = None
	centroid = None


def fortune(shape, provinces, wrap=None):
	wrap = (0,0) if wrap is None else wrap
	by_x = sorted([(p[1], i, p) for i, p in enumerate(provinces)])
	by_y = sorted([(p[0], i, p) for i, p in enumerate(provinces)])

	for y in range(shape[0]):
		if not by_y: break
		elif y > by_y[0][0]: print(y, by_y.pop(0))

	eventqueue = []

def scipykludge(shape, provinces, wrap=None):
	wrap = (0,0) if wrap is None else wrap

	fullprovinces = []
	fullprovinces.extend(provinces)
	if wrap[0]: 
		fullprovinces.extend([p - [shape[0],0] for p in provinces])
		fullprovinces.extend([p + [shape[0],0] for p in provinces])
	else:
		fullprovinces.extend([p * [-1, 1] for p in provinces])
		fullprovinces.extend([(p * [-1, 1]) + [2*shape[0],0] for p in provinces])
	if wrap[1]: 
		fullprovinces.extend([p - [0,shape[1]] for p in provinces])
		fullprovinces.extend([p + [0,shape[1]] for p in provinces])
	else:
		fullprovinces.extend([p * [ 1,-1] for p in provinces])
		fullprovinces.extend([(p * [ 1,-1]) + [0,2*shape[1]] for p in provinces])

	if wrap[0] and wrap[1]:
		fullprovinces.extend([p + [-shape[0],-shape[1]] for p in provinces])
		fullprovinces.extend([p + [-shape[0],+shape[1]] for p in provinces])
		fullprovinces.extend([p + [+shape[0],-shape[1]] for p in provinces])
		fullprovinces.extend([p + [+shape[0],+shape[1]] for p in provinces])

	elif wrap[0] and not wrap[1]: 
		fullprovinces.extend([p * [ 1,-1] + [-shape[0],0] for p in provinces])
		fullprovinces.extend([p * [ 1,-1] + [+shape[0],0] for p in provinces])
		fullprovinces.extend([p * [ 1,-1] + [-shape[0],2*shape[0]] for p in provinces])
		fullprovinces.extend([p * [ 1,-1] + [+shape[0],2*shape[0]] for p in provinces])

	elif wrap[1] and not wrap[0]: 
		fullprovinces.extend([p * [-1, 1] + [0,-shape[1]] for p in provinces])
		fullprovinces.extend([p * [-1, 1] + [0,+shape[1]] for p in provinces])
		fullprovinces.extend([p * [-1, 1] + [2*shape[0],-shape[1]] for p in provinces])
		fullprovinces.extend([p * [-1, 1] + [2*shape[0],+shape[1]] for p in provinces])
	else: 
		fullprovinces.extend([-p for p in provinces])
		fullprovinces.extend([-p + [shape[0]*2,0] for p in provinces])
		fullprovinces.extend([-p + [0,shape[1]*2] for p in provinces])
		fullprovinces.extend([-p + [shape[0]*2,shape[1]*2] for p in provinces])


	vor = Voronoi(fullprovinces)

	#plt.plot([p[1] for p in fullprovinces], [p[0] for p in fullprovinces], lw=0, marker='.')
	#voronoi_plot_2d(vor)
	#plt.show()
	#return
	provgonlist = []
	for pointindex, regionindex in enumerate(vor.point_region):
		provindex = pointindex % len(provinces)

		##this little hack should reduce runtime by 50% or so
		if not (-shape[0]/2 <= vor.points[pointindex][0] <= shape[0]*3/2): continue
		if not (-shape[1]/2 <= vor.points[pointindex][1] <= shape[1]*3/2): continue


		vertlist = [vor.vertices[i][::-1] for i in vor.regions[regionindex]]
		provgon = Provincegon(vertlist)
		provgon.set_facecolor('#{:06x}'.format(hash(str(provindex)) % 2**24))
		#provgon.set_facecolor(np.random.random(3))
		provgon.provid = provindex
		provgon.centroid = np.mean(vertlist, axis=0)
		provgonlist.append(provgon)


	#ax = plt.figure().gca()
	#for pgon in provgonlist:
	#	ax.add_patch(pgon)
	#	ax.annotate('{}'.format(pgon.provid), pgon.centroid)
	#ax.set_xlim([0, shape[1]])
	#ax.set_ylim([0, shape[0]])
	#plt.show()


	voronoi = -np.ones(shape)
	return fill_en_bloc(shape, provgonlist, voronoi), vor

def fill_en_bloc(shape, provgonlist, voronoi):
	#print(shape)
	start = time.time()
	for provgon in provgonlist:
		unfilled = np.roll(np.vstack(np.nonzero(voronoi == -1)).T, 1, 1)

		fillme = unfilled[provgon.contains_points(unfilled)]
		voronoi[fillme[:,1], fillme[:,0]] = provgon.provid
	#print(time.time() - start)
	return voronoi

if __name__ == '__main__':
	shape = (1000, 2000)

	density = np.random.random(shape)

	fft = np.fft.fft2(density)
	mask = np.zeros(shape)
	mask[:1,:2] =   150
	mask[:1,-2:] =  150
	mask[-1:,:2] =  150
	mask[-1:,-2:] = 150
	mask[0,0] = 0.5# * shape[0] * shape[1]
	fft *= mask

	smoothdensity = np.real(np.fft.ifft2(fft))

	provincesize = np.ones(shape)
	provincesize[smoothdensity < 0.25] = 3.5

	wrap=(0,1)
	provinces = bluenoise.place_n(shape, provincesize, npoints=100, wrap=wrap)

	voronoi = scipykludge(shape, provinces, wrap=wrap)
	plt.imshow(voronoi)
	plt.xlim([0, shape[1]])
	plt.ylim([0, shape[0]])
	plt.show()


	exit()

	x, y = [], []
	for p in provinces:
		x.append(p[1])
		y.append(p[0])
	plt.plot(x, y, lw=0, marker='.')
	plt.imshow(provincesize)
	plt.colorbar()
	plt.show()

#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.path import Path
import numpy as np
import json
from PIL import Image
import bluenoise
from scipy import ndimage
import os
from astropy.convolution import kernels, convolve
import argparse

seeds = list(np.random.randint(0, 2**32-1, 500))

def paint_land(elevation, canvas=None, winter=False):
	if canvas is None: canvas = np.zeros(elevation.shape + (3,))
	#canvas[elevation < 0.125] = np.array([0.625, 0.75, 0.8125])
	red = np.zeros(elevation.shape)
	green = np.zeros(elevation.shape)
	blue = np.zeros(elevation.shape)

	if winter:
		red[elevation <= 1.0] =   0.75
		green[elevation <= 1.0] = 0.65
		blue[elevation <= 1.0] =  0.65

		red[elevation < 0.75] =   0.95
		green[elevation < 0.75] = 0.85
		blue[elevation < 0.75] =  0.70

		red[elevation < 0.75] =   0.85
		green[elevation < 0.75] = 0.85
		blue[elevation < 0.75] =  0.80
		
	else:
		red[elevation <= 1.0] =   0.75
		green[elevation <= 1.0] = 0.65
		blue[elevation <= 1.0] =  0.65

		red[elevation < 0.75] =   0.95
		green[elevation < 0.75] = 0.85
		blue[elevation < 0.75] =  0.70

		red[elevation < 0.75] =   0.85
		green[elevation < 0.75] = 0.85
		blue[elevation < 0.75] =  0.60



	canvas += np.stack([red,green,blue], axis=2)

	return canvas

def paint_sea(elevation, canvas=None):
	if canvas is None: canvas = np.zeros(elevation.shape + (3,))
	#canvas[elevation < 0.125] = np.array([0.625, 0.75, 0.8125])
	red = canvas[:,:,0]
	green = canvas[:,:,1]
	blue = canvas[:,:,2]

	red[elevation < 0.25] =   0.85
	green[elevation < 0.25] = 0.90
	blue[elevation < 0.25] =  0.95

	red[elevation < 0.2] =    0.70
	green[elevation < 0.2] =  0.80
	blue[elevation < 0.2] =   0.90

	red[elevation < 0.125] =   0.60
	green[elevation < 0.125] = 0.70
	blue[elevation < 0.125] =  0.80

	return canvas

def repair_rivers(shape, rivers, bridges, threshold=0.5):
	outrivers = []
	sqthr = [s*threshold for s in shape]

	head = {}
	tail = {}
	rivertype = {}
	for river in rivers:
		for i in range(len(river)-1):
			start = tuple(river[i])
			end = tuple(river[i+1])
			try: head[start] += 1
			except KeyError: head[start] = 1
			if start not in tail: tail[start] = 0
		
			try: tail[end] += 1
			except KeyError: tail[end] = 1
			if end not in head: head[end] = 0

	for p in head: rivertype[p] = 0 #0: normal, 1: source, 2: wide, 3: mouth

	source = set()
	confluence = set()
	big = set()
	for p in head: 
		if head[p] > 1: confluence.add(p)
		elif tail[p] == 0: source.add(p)
	mouth = set()
	for p in tail:
		if head[p] == 0: mouth.add(p)

	for river in rivers:
		wide = False
		for vertex in river:
			p = tuple(vertex)
			if p in confluence: wide = True

			if wide: big.add(p)

	arrbridges = []
	for bridge in bridges: 
		arrbridges.append([tuple(p) for p in bridge])

	outbridges = []
	outsegments = []
	rivertypes = []
	for river in rivers:
		outriver = []
		vsplit = []
		hsplit = []
		last = None
		for i in range(len(river)):
			current = np.array(river[i])

			if last is None:
				last = current
				continue
			vec = current - last
			vsplit = False
			hsplit = False

			if abs(vec[0]) > (threshold*shape[0]): vsplit = True
			if abs(vec[1]) > (threshold*shape[1]): hsplit = True

			l = tuple(last)
			c = tuple(current)
			rt = 0
			if l in source: rt += 1
			elif c in mouth: rt += 4
			if l in big: rt += 2
			rivertypes.append(rt)

			if vsplit and hsplit: pass
			elif vsplit:
				rivertypes.append(rt)
				if last[0] < current[0]:
					seg1 = [last, current - [shape[0],0]]
					seg2 = [last + [shape[0],0], current]
					outsegments.extend([seg1, seg2])
					#print([tuple(last), tuple(current)], [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges)
					if [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges: 
						outbridges.extend([seg1, seg2])
						print('split bridge at {} to {} {}'.format([last, current], seg1, seg2))
				else:
					seg1 = [last, current + [shape[0],0]]
					seg2 = [last - [shape[0],0], current]
					outsegments.extend([seg1, seg2])
					#print([tuple(last), tuple(current)], [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges)
					if [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges: 
						outbridges.extend([seg1, seg2])
						print('split bridge at {} to {} {}'.format([last, current], seg1, seg2))
			elif hsplit:
				rivertypes.append(rt)
				if last[1] < current[1]:
					seg1 = [last, current - [0,shape[1]]]
					seg2 = [last + [0,shape[1]], current]
					outsegments.extend([seg1, seg2])
					#print([tuple(last), tuple(current)], [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges)
					if [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges: 
						outbridges.extend([seg1, seg2])
						print('split bridge at {} to {} {}'.format([last, current], seg1, seg2))
				else:
					seg1 = [last, current + [0,shape[1]]]
					seg2 = [last - [0,shape[1]], current]
					outsegments.extend([seg1, seg2])
					#print([tuple(last), tuple(current)], [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges)
					if [tuple(last), tuple(current)] in arrbridges or [tuple(current), tuple(last)] in arrbridges: 
						outbridges.extend([seg1, seg2])
						print('split bridge at {} to {} {}'.format([last, current], seg1, seg2))
			else:
				if last is not None: 
					outsegments.append([last, current])
					if [tuple(last),tuple(current)] in arrbridges: outbridges.append([last,current])
			

			last = current
		if last is not None: 
			outriver.append(last)

	[print(bridge) for bridge in outbridges]
	#for seg in outsegments: print(seg)
	return outsegments, rivertypes, outbridges

def _swiz(num, place): return (num>>place) % 2

def midpoint_displacement(rivers, elevation=None):
	outrivers = []
	for river in rivers:
		outriver = []
		for i in range(len(river)-1):
			start = np.array(river[i])
			end = np.array(river[i+1])

			
			mp = np.mean([start, end], axis=0)
			vec = end - start
			normvec = vec[::-1] * [1,-1]

				
			amount = np.random.random() * 0.4
			poss1 = mp + normvec * amount
			poss2 = mp + normvec * -amount
			outriver.append(start)
			
			if elevation is None:
				outriver.append([poss1,poss2][np.random.randint(2)])
			else:
				startelev = elevation[tuple(np.int64(start))]
				endelev = elevation[tuple(np.int64(end))]

				elev1 = elevation[tuple(np.int64(poss1))]
				elev2 = elevation[tuple(np.int64(poss2))]

				if (elev1 <= 0.25) and (elev2 <= 0.25): pass
				elif (elev1 <= 0.25): outriver.append(poss2)
				elif (elev2 <= 0.25): outriver.append(poss1)
				elif (startelev >= elev1 >= endelev): outriver.append(poss1)
				elif (startelev >= elev2 >= endelev): outriver.append(poss2)
				else: pass

		outriver.append(river[-1])

		outrivers.append(outriver)
	return outrivers
				

def paint_rivers(elevation, rivers, rivertypes, canvas, riverwidth=2):
	land = np.roll(np.vstack(np.nonzero(elevation > 0.25)).T, 1, axis=1)

	red = canvas[:,:,0]
	green = canvas[:,:,1]
	blue = canvas[:,:,2]

	#rivers = midpoint_displacement(rivers, elevation)
	#rivers = midpoint_displacement(rivers, elevation)

	patchlist = []
	
	seglist = []
	for river, rivertype in zip(rivers, rivertypes):
		for i in range(len(river)-1):
			start = np.array(river[i][::-1])
			end = np.array(river[i+1][::-1])
			seglist.append((tuple(start), tuple(end)))
			vec = end - start
			vec /= np.linalg.norm(vec)

			normvec = vec[::-1] * [-1,1]

			pt1 = start + riverwidth * (+normvec - vec)
			pt2 = start + riverwidth * (-normvec - vec)
			pt3 = end + riverwidth * (-normvec + vec)
			pt4 = end + riverwidth * (+normvec + vec)


			if _swiz(rivertype, 0):
				pt1 += riverwidth * (-normvec)
				pt2 += riverwidth * (+normvec)

			if _swiz(rivertype, 1): #wide
				pt1 += riverwidth * normvec
				pt2 += riverwidth * -normvec
				pt3 += riverwidth * -normvec
				pt4 += riverwidth * normvec

			if _swiz(rivertype, 2):
				pt3 += riverwidth * (-3*normvec - 3*vec)
				pt4 += riverwidth * (3*normvec - 3*vec)


			rectcorners = [ pt1,pt2,pt3,pt4 ]
			patch = patches.Polygon(rectcorners, fc=(0.85,0.90,0.95))
			#ax.add_patch(patch)
			match = land[patch.contains_points(land)]
			#print(start, end)
			red[match[:,1], match[:,0]] =   0.85
			green[match[:,1], match[:,0]] = 0.90
			blue[match[:,1], match[:,0]] =  0.95

	eq = 0
	for i, x in enumerate(seglist):
		for j, y in enumerate(seglist):
			if i > j: continue
			if x == y: eq += 1
	#print(eq, len(seglist), len(rivertypes))

	return canvas

def seed_mountains(elevation, provincemap, highlandlist):
	molehills = []
	for i, x in enumerate(highlandlist):
		if x:
			posspoints = np.vstack(np.nonzero((provincemap == i) & (elevation > 0.625))).T
			if not len(posspoints): 
				highestquarter = np.percentile(elevation[provincemap == i], 75)
				posspoints = np.vstack(np.nonzero((provincemap == i) & (elevation >= highestquarter))).T
				if not len(posspoints): continue
			molehills.append(posspoints[np.random.randint(posspoints.shape[0])])
	return molehills

def seed_hills(provincemap, highlandlist):
	hillocks = []
	for i, x in enumerate(highlandlist):
		if x:
			posspoints = np.vstack(np.nonzero(provincemap == i)).T
			hillocks.append(posspoints[np.random.randint(posspoints.shape[0])])

	return hillocks

def seed_forests(provincemap, forestdensity, forestlist):
	saplings = []
	for i, x in enumerate(forestlist):
		if x or (np.mean(forestdensity[provincemap == i]) >= 0.3): 
			posspoints = np.vstack(np.nonzero(provincemap == i)).T
			saplings.append(posspoints[np.random.randint(posspoints.shape[0])])

	return saplings

def seed_grass(provincemap, grassdensity, grasslist):
	seedlings = []
	for i, x in enumerate(grasslist):
		if x: 
			posspoints = np.vstack(np.nonzero(provincemap == i)).T
			seedlings.append(posspoints[np.random.randint(posspoints.shape[0])])

	return seedlings

def seed_thing(provincemap, density, provlist):
	sites = []
	for i, x in enumerate(provlist):
		if x:
			posspoints = np.vstack(np.nonzero(provincemap == i)).T
			sites.append(posspoints[np.random.randint(posspoints.shape[0])])
	return sites

def get_mountain_density(elevation, provincemap, provincelist):
	mountaindensity = np.ones(elevation.shape)
	mountaindensity[elevation <= 1] = 1.0
	mountaindensity[elevation <= 0.875] = 0.8
	mountaindensity[elevation <= 0.75] = 0.3
	mountaindensity[elevation <= 0.625] = 0.2
	mountaindensity[elevation <= 0.5] = 0.1

	for i, x in enumerate(provincelist):
		if x: pass
		else: mountaindensity[provincemap == i] *= 0.5

	mountaindensity[elevation < 0.25] = 0.02
	
	return mountaindensity

def get_hill_density(elevation, provincemap, provincelist):
	hilldensity = np.ones(elevation.shape)
	hilldensity[elevation <= 1] = 0.5
	hilldensity[elevation < 0.75] = 1
	hilldensity[elevation < 0.5] = 0.5

	for i, x in enumerate(provincelist):
		if x: pass
		else: hilldensity[provincemap == i] *= 0.1

	hilldensity[elevation < 0.25] = 0.02
	
	return hilldensity

def get_grass_density(elevation, provincemap, mapcodes):
	grassdensity = np.ones(elevation.shape)
	grassdensity[elevation > 0.375] = 0.8
	for i, x in enumerate(mapcodes):
		if _swiz(x, 1): grassdensity[provincemap == i] *= 0.5
		if _swiz(x, 8): grassdensity[provincemap == i] *= 0.5
		if _swiz(x, 16): grassdensity[provincemap == i] *= 0.1

	grassdensity[elevation < 0.25] = 0.02
	grassdensity[elevation > 0.75] = 0.02
	for i, x in enumerate(mapcodes):
		if _swiz(x, 6): grassdensity[provincemap == i] = 0.2
	return grassdensity

def get_grass_type(provincemap, mapcodes):
	#ft: {0: temperate, 1: swamp, 2: desert}
	grasstype = np.zeros(provincemap.shape)
	for i, x in enumerate(mapcodes):
		if _swiz(x, 5): grasstype[provincemap == i] = 1
		elif _swiz(x, 6): grasstype[provincemap == i] = 2
	return grasstype

def place_mountains(elevation, mountaindensity, canvas, molehills=None):
	mountainpoints = bluenoise.place_many(elevation.shape, mountaindensity, base_mindist=31, attempts=30, wrap=(1,1), initpoints=molehills)
	return mountainpoints

def place_hills(elevation, hilldensity, canvas, hillocks=None):
	hillpoints = bluenoise.place_many(elevation.shape, hilldensity, base_mindist=15, attempts=30, wrap=(1,1), initpoints=hillocks)
	return hillpoints

def place_forests(elevation, foresttype, forestdensity, canvas, saplings=None):
	forestdensity = np.clip(forestdensity, 0.02, 1)
	treepoints = bluenoise.place_many(elevation.shape, forestdensity, base_mindist=6, attempts=30, wrap=(1,1), initpoints=saplings)
	return treepoints 

def place_grass(elevation, grasstype, grassdensity, canvas, seedlings=None):
	grassdensity = np.clip(grassdensity, 0.02, 1)
	grasspoints = bluenoise.place_many(elevation.shape, grassdensity, base_mindist=7, attempts=30, wrap=(1,1), initpoints=seedlings)
	return grasspoints 

def place_thing(thingdensity, mindist=16, sites=None, strict=False):
	thingdensity2 = np.clip(thingdensity, 0.02, 1)
	points = bluenoise.place_many(thingdensity2.shape, thingdensity2, base_mindist=mindist, attempts=30, wrap=(1,1), initpoints=sites)

	if strict:
		truepoints = []
		for p in points:
			if thingdensity[tuple(np.int64(p))] != 0: truepoints.append(p)
		return truepoints
	else: return points

def load_sprites(indir, categories, winter=False):
	spritedict = {}
	for category in categories: 
		if winter: spritedict['winter{}'.format(category)] = []
		else: spritedict[category] = []
	for bn in sorted(os.listdir(indir)):
		for category in spritedict:
			if bn.startswith(category):
				with Image.open('{}/{}'.format(indir, bn)) as im: spritedict[category].append(np.float64(im)/255)
	return spritedict


def load_tree_sprites(indir, winter=False): return load_sprites(indir, ['conifer', 'deciduous', 'seaweed'], winter=winter)

def load_mountain_sprites(indir): return load_sprites(indir, ['mountain'])

def load_hill_sprites(indir, winter=False): return load_sprites(indir, ['hill'], winter=winter)

def load_grass_sprites(indir, winter=False): return load_sprites(indir, ['swamp', 'temperate', 'seaweed', 'desert'], winter=winter)

def test_paint_land(elevation, canvas=None, winter=False):
	canvas = np.zeros((elevation.shape + (3,))) if canvas is None else canvas
	paint_land(elevation, canvas, winter=winter)

	return canvas

def test_paint_provinces(elevation, provinces, mapcodes, canvas=None, winter=False):
	canvas = np.zeros((provinces.shape + (3,))) if canvas is None else canvas

	land = elevation > 0.25

	red =   canvas[:,:,0]
	green = canvas[:,:,1]
	blue =  canvas[:,:,2]

	highland = [_swiz(num, 4) for num in mapcodes]
	swamp = [_swiz(num, 5) for num in mapcodes]
	waste = [_swiz(num, 6) for num in mapcodes]
	forest = [_swiz(num, 7) for num in mapcodes]

	if winter:
		for i in range(len(mapcodes)):
			if highland[i]:
				red[(provinces == i) & land] =   0.80
				green[(provinces == i) & land] = 0.75
				blue[(provinces == i) & land] =  0.75
			if swamp[i]:
				red[(provinces == i) & land] =   0.70
				green[(provinces == i) & land] = 0.75
				blue[(provinces == i) & land] =  0.65
			if waste[i]:
				red[(provinces == i) & land] =   0.90
				green[(provinces == i) & land] = 0.90
				blue[(provinces == i) & land] =  0.85
			if forest[i]:
				red[(provinces == i) & land] =   0.75
				green[(provinces == i) & land] = 0.75
				blue[(provinces == i) & land] =  0.75
	else:
		for i in range(len(mapcodes)):
			if highland[i]:
				red[(provinces == i) & land] =   0.85
				green[(provinces == i) & land] = 0.80
				blue[(provinces == i) & land] =  0.70
			if swamp[i]:
				red[(provinces == i) & land] =   0.70
				green[(provinces == i) & land] = 0.75
				blue[(provinces == i) & land] =  0.60
			if waste[i]:
				red[(provinces == i) & land] =   0.95
				green[(provinces == i) & land] = 0.85
				blue[(provinces == i) & land] =  0.70
			if forest[i]:
				red[(provinces == i) & land] =   0.85
				green[(provinces == i) & land] = 0.85
				blue[(provinces == i) & land] =  0.55

	kernel = kernels.Gaussian2DKernel(x_stddev=12, y_stddev=12, x_size=32*2+1, y_size=32*2+1)
	red = convolve(red, kernel, boundary='wrap')
	green = convolve(green, kernel, boundary='wrap')
	blue = convolve(blue, kernel, boundary='wrap')

	canvas = np.stack([red,green,blue], axis=2)

	return canvas

def test_paint_rivers(elevation, rivers, rivertypes, canvas=None):
	canvas = np.zeros((elevation.shape + (3,))) if canvas is None else canvas

	return paint_rivers(elevation, rivers, rivertypes, canvas)

def test_place_forests(elevation, treepoints, foresttype, winter=False):

	imgdict = load_tree_sprites('sprites/trees', winter=winter)

	queue = []
	for pt in treepoints:
		ft = foresttype[tuple(np.int64(pt))]
		ptelev = elevation[tuple(np.int64(pt))]
		if winter:
			if ft == 1: spritebank = imgdict['winterconifer']
			elif ft == 3: spritebank = [imgdict['winterconifer'], imgdict['winterdeciduous']][np.random.randint(2)]
			elif ptelev < 0.25: spritebank = imgdict['winterseaweed']
			else: spritebank = imgdict['winterdeciduous']
		else:
			if ft == 1: spritebank = imgdict['conifer']
			elif ft == 3: spritebank = [imgdict['conifer'], imgdict['deciduous']][np.random.randint(2)]
			elif ptelev < 0.25: spritebank = imgdict['seaweed']
			else: spritebank = imgdict['deciduous']
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite])

	return queue

def test_place_mountains(elevation, mountainpoints, winter=False):
	imgdict = load_mountain_sprites('sprites/mountains')

	queue = []
	for pt in mountainpoints:
		elev = elevation[tuple(np.int64(pt))]
		if elev < 0.375: continue
		else: spritebank = imgdict['mountain']
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite])

	return queue

def test_place_hills(elevation, hillpoints, winter=False):
	imgdict = load_hill_sprites('sprites/hills', winter=winter)

	queue = []
	for pt in hillpoints:
		elev = elevation[tuple(np.int64(pt))]
		if elev < 0.375: continue
		elif winter: spritebank = imgdict['winterhill']
		else: spritebank = imgdict['hill']
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite])

	return queue

def test_place_grass(elevation, grasspoints, grasstype, seedlings=None, winter=False):
	imgdict = load_grass_sprites('sprites/grass', winter=winter)

	#ft: {0: temperate, 1: swamp, 2: desert}
	queue = []
	for pt in grasspoints:
		ft = grasstype[tuple(np.int64(pt))]
		ptelev = elevation[tuple(np.int64(pt))]
		if winter:
			if ptelev < 0.25: spritebank = imgdict['winterseaweed']
			elif ft == 0: spritebank = imgdict['wintertemperate']
			elif ft == 1: spritebank = imgdict['winterswamp']
			elif ft == 2: spritebank = imgdict['winterdesert']
			elif ptelev >= 0.5: continue
			else: continue
		else:
			if ptelev < 0.25: spritebank = imgdict['seaweed']
			elif ft == 0: spritebank = imgdict['temperate']
			elif ft == 1: spritebank = imgdict['swamp']
			elif ft == 2: spritebank = imgdict['desert']
			elif ptelev >= 0.5: continue
			else: continue
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite, True])

	return queue

def test_place_farms(elevation, farmpoints, winter=False):
	imgdict = load_sprites('sprites/farm', ['field'], winter=winter)

	queue = []
	spritebank = imgdict['winter'*winter + 'field']
	for pt in farmpoints:
		ptelev = elevation[tuple(np.int64(pt))]
		if ptelev < 0.25: continue
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite, True])
	return queue

def test_place_caves(elevation, cavepoints, winter=False):
	imgdict = load_sprites('sprites/caves', ['cave'], winter=winter)

	queue = []
	spritebank = imgdict['winter'*winter + 'cave']
	for pt in cavepoints:
		ptelev = elevation[tuple(np.int64(pt))]
		if ptelev < 0.25: continue
		sprite = spritebank[np.random.randint(len(spritebank))]
		queue.append([pt, sprite, True])
	return queue

def test_place_towns(elevation, townpoints, provinces, biomecodes, winter=False):
	imgdict = load_sprites('sprites/towns', ['town'], winter=winter)

	queue = []
	spritebank = imgdict['town']
	for pt in townpoints:
		ptelev = elevation[tuple(np.int64(pt))]
		biome = biomecodes[provinces[tuple(np.int64(pt))]]
		if ptelev < 0.25: continue #watertowns!
		spriteindex = np.random.randint(2)
		if _swiz(biome, 0): spriteindex -= 1
		elif _swiz(biome, 1): spriteindex += 2
		else: spriteindex += 1

		if _swiz(biome, 4): spriteindex -= 0 #highlands
		if _swiz(biome, 5): spriteindex -= 2 #swamp
		if _swiz(biome, 6): spriteindex -= 2 #waste
		if _swiz(biome, 22): spriteindex -= 1 #mountain

		if _swiz(biome, 7): spriteindex -= 0 #forest, forest caves
		elif _swiz(biome, 12): spriteindex -= 1 #caves
		elif _swiz(biome, 22): spriteindex -= 0

		sprite = spritebank[np.clip(spriteindex, 0, 3)]
		queue.append([pt, sprite, True])
	return queue

def test_place_bridges(segments, winter=False):
	imgdict = load_sprites('sprites/bridges', ['bridge'], winter=winter)
	spritebank = imgdict['winter'*winter + 'bridge']
	queue = []
	for start, end in segments:
		p1 = np.array(start)
		p2 = np.array(end)
		vec = p2 - p1
		mp = (p1 + p2)/2

		plt.plot([p1[1], p2[1]], [p1[0], p2[0]])
		#plt.scatter(mp[0], mp[1], color='r', marker='x')
		#plt.scatter(mp[1], mp[0], color='r', marker='x')
		#plt.scatter(-mp[1], -mp[0], color='r', marker='o')
		#plt.scatter(-mp[0], -mp[1], color='r', marker='o')

		angle = np.arctan(vec[0]/vec[1]) * 180/np.pi
		bridgeid = None
		if angle >= 60: bridgeid = 2
		elif angle >= 30: bridgeid = 1
		elif angle >= 0: bridgeid = 0
		elif angle >= -30: bridgeid = 5
		elif angle >= -60: bridgeid = 4
		elif angle >= -90: bridgeid = 3
		plt.annotate(bridgeid, (mp[1], mp[0]))
		sprite = spritebank[bridgeid]
		#queue.append([mp[::-1], sprite])
		queue.append([mp, sprite, 'center'])

	return queue
def test_paint_coastline(canvas):
	shallow = (canvas[:,:,0] == 0.85) & (canvas[:,:,1] == 0.90) & (canvas[:,:,2] == 0.95) 
	middeep = (canvas[:,:,0] == 0.70) & (canvas[:,:,1] == 0.80) & (canvas[:,:,2] == 0.90) 
	verydeep =(canvas[:,:,0] == 0.60) & (canvas[:,:,1] == 0.70) & (canvas[:,:,2] == 0.80) 
	is_not_water = ~(shallow | middeep | verydeep)

	near_coast = np.zeros(shallow.shape, dtype=np.uint8)
	radius = 2
	rad2 = radius**2
	for r in range(-radius, radius+1):
		rolled = np.roll(shallow, r, axis=0)
		for c in range(-radius, radius+1):
			if r**2 + c**2 > rad2: continue
			near_coast = near_coast | np.roll(rolled, c, axis=1)
	coastline = (near_coast & is_not_water) > 0

	r = canvas[:,:,0]
	g = canvas[:,:,1]
	b = canvas[:,:,2]
	r[coastline] = 0.60
	g[coastline] = 0.50
	b[coastline] = 0.40

	canvas = np.stack([r,g,b], axis=2)

	return canvas

def test_paint_borders(provinces, canvas):
	diff = np.zeros(provinces.shape, dtype=np.bool8)
	for r in range(-1, 1+1):
		rolled = np.roll(provinces, r, axis=0)
		for c in range(-1, 1+1):
			diff = diff | (provinces != np.roll(rolled, c, axis=1))
	r = canvas[:,:,0]
	g = canvas[:,:,1]
	b = canvas[:,:,2]
	r[diff] = r[diff]**0.75
	g[diff] = g[diff]**0.75
	b[diff] = b[diff]**0.75
	return canvas

def test_paint_centers(centers, canvas):
	pix = np.ones((1,1,3))
	shape = canvas.shape[:2]
	for i in range(len(centers)):
		y, x = centers[i]
		if (0 <= y < shape[0]) and (0 <= x < shape[1]): 
			canvas[int(y),int(x),:] = pix
	return centers

def blit(img, canvas, pos=(0,0), va='baseline', ha='center'):
	img = img.copy()
	if img.shape[2] == 4:
		img = img[:,:,:3]
	if va == 'baseline': dr = -img.shape[0]
	elif va == 'top': dr = 0
	elif va == 'center': dr = -img.shape[0]//2
	elif va == 'baseline': dr = -img.shape[0]
	else: raise ValueError

	if ha == 'left': dc = 0	
	elif ha == 'center': dc = -img.shape[1]//2
	elif ha == 'right': dc = -img.shape[1]
	else: raise ValueError

	topleft = int(pos[0]+dr),int(pos[1]+dc)

	target = canvas[topleft[0],topleft[1],:]
	target = canvas[topleft[0]:topleft[0]+img.shape[0],topleft[1]:topleft[1]+img.shape[1],:]

	mask = (img[:,:,0] == 1) & (img[:,:,1] == 0) & (img[:,:,2] == 1)
	try: img[mask] = target[mask]
	except IndexError: 
		for r in range(img.shape[0]):
			cur_r = (topleft[0] + r) % canvas.shape[0]
			for c in range(img.shape[1]):
				cur_c = (topleft[1] + c) % canvas.shape[1]
				if not mask[r,c]: canvas[cur_r,cur_c,:] = img[r,c,:]
		return canvas#??? probably painting on map edge

	canvas[topleft[0]:topleft[0]+img.shape[0],topleft[1]:topleft[1]+img.shape[1],:] = img
	return canvas

def blit_many(queue, canvas):
	realqueue = []
	for item in queue:
		if len(item) == 2: 
			p, img = item
			center = False
		else: p, img, center = item
		realqueue.append((tuple(p), np.random.random(), img, center))
	for p, nonce, img, center in sorted(realqueue):
		if center: blit(img, canvas, p, va='center')
		else: blit(img, canvas, p)
	return canvas

def roll_neighbors(provinces):
	pairs = set()
	#rolldown
	rolldown = np.roll(provinces, 1, axis=0)
	mismatch = (provinces != rolldown)
	collected = np.vstack([np.ravel(provinces[mismatch]), np.ravel(rolldown[mismatch])]).T
	for row in collected:
		if row[0] != row[1]: pairs.add(tuple(sorted(row)))

	rollright = np.roll(provinces, 1, axis=1)
	mismatch = (provinces != rollright)
	collected = np.vstack([np.ravel(provinces[mismatch]), np.ravel(rollright[mismatch])]).T
	for row in collected:
		if row[0] != row[1]: pairs.add(tuple(sorted(row)))

	return list(pairs)


def main(outprefix):
	print('painting land')
	with Image.open('elevation.png') as im: elevation = np.float64(im)/255
	summercanvas = test_paint_land(elevation)
	wintercanvas = test_paint_land(elevation, winter=True)

	with Image.open('provinces.png') as im: provinces = np.array(im)
	neighbors = roll_neighbors(provinces)

	print('painting provinces')
	with open('biomes.txt') as fh: biomecodes = json.load(fh)
	summercanvas = test_paint_provinces(elevation, provinces, biomecodes, summercanvas)
	wintercanvas = test_paint_provinces(elevation, provinces, biomecodes, wintercanvas, winter=True)

	with open('centers.txt') as fh: centers = json.load(fh)

	print('painting seas')	
	paint_sea(elevation, summercanvas)
	paint_sea(elevation, wintercanvas)
	summerqueue = []
	winterqueue = []

	#rivers
	print('painting rivers')
	with open('rivers.txt') as fh: hydro = json.load(fh)
	rivers = hydro['rivers']
	#bridges = hydro['bridges']
	rivers, rivertypes, bridges = repair_rivers(elevation.shape, rivers, hydro['bridges'])
	test_paint_rivers(elevation, rivers, rivertypes, summercanvas)
	test_paint_rivers(elevation, rivers, rivertypes, wintercanvas)

	#coastline
	print('painting coastline')
	test_paint_coastline(summercanvas)
	test_paint_coastline(wintercanvas)

	#hills
	hillocks = seed_hills(provinces, [_swiz(code, 4) for code in biomecodes])
	hilldensity = get_hill_density(elevation, provinces, [_swiz(code, 4) for code in biomecodes])
	hillpoints = place_hills(elevation, hilldensity, summercanvas, hillocks)



	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_hills(elevation, hillpoints))
	np.random.seed(seed)
	winterqueue.extend(test_place_hills(elevation, hillpoints, winter=True))

	#mountains
	print('painting mountains')
	molehills = seed_mountains(elevation, provinces, [_swiz(code, 4) or _swiz(code, 22) for code in biomecodes])
	mountaindensity = get_mountain_density(elevation, provinces, [_swiz(code, 4) or _swiz(code, 22) for code in biomecodes])
	mountainpoints = place_mountains(elevation, mountaindensity, summercanvas, molehills)
	#[print(q) for q in sorted([tuple(p) for p in mountainpoints])]

	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_mountains(elevation, mountainpoints))
	np.random.seed(seed)
	winterqueue.extend(test_place_mountains(elevation, mountainpoints, winter=True))


	#forests
	with Image.open('foresttype.png') as im: foresttype = np.array(im)
	with Image.open('forestdensity.png') as im: forestdensity = np.float64(im)/255
	bigdensity = ndimage.zoom(forestdensity, (summercanvas.shape[0]/forestdensity.shape[0], summercanvas.shape[1]/forestdensity.shape[1]), order=3)
	bigtypes = ndimage.zoom(foresttype, (summercanvas.shape[0]/forestdensity.shape[0], summercanvas.shape[1]/forestdensity.shape[1]), order=0)
	saplings = seed_forests(provinces, bigdensity, [_swiz(code, 7) for code in biomecodes])
	treepoints = place_forests(elevation, bigtypes, bigdensity, summercanvas, saplings)

	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_forests(elevation, treepoints, bigtypes))
	np.random.seed(seed)
	winterqueue.extend(test_place_forests(elevation, treepoints, bigtypes, winter=True))

	#grass
	grassprovinces = [not (_swiz(code, 16) | _swiz(code, 6) | _swiz(code, 4)) for code in biomecodes]
	grasstype = get_grass_type(provinces, biomecodes)
	grassdensity = get_grass_density(elevation, provinces, biomecodes)
	seedlings = seed_grass(provinces, grassdensity, grassprovinces)
	grasspoints = place_grass(elevation, grasstype, grassdensity, summercanvas, seedlings)

	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_grass(elevation, grasspoints, grasstype))
	np.random.seed(seed)
	winterqueue.extend(test_place_grass(elevation, grasspoints, grasstype, winter=True))

	#farms
	farmdensity = np.zeros(provinces.shape)
	initfarms = seed_thing(provinces, farmdensity, [_swiz(code, 8) for code in biomecodes])
	for i, code in enumerate(biomecodes):
		if _swiz(code, 8): farmdensity[provinces == i] = 1
	farmpoints = place_thing(farmdensity, mindist=64, sites=initfarms)

	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_farms(elevation, farmpoints))
	np.random.seed(seed)
	winterqueue.extend(test_place_farms(elevation, farmpoints, winter=True))
	
	#bridges
	summerqueue.extend(test_place_bridges(bridges))
	winterqueue.extend(test_place_bridges(bridges, winter=True))

	#caves
	print('placing caves')
	cavedensity = np.zeros(provinces.shape)
	initcaves = seed_thing(provinces, cavedensity, [_swiz(code, 12) for code in biomecodes])
	for i, code in enumerate(biomecodes):
		if _swiz(code, 12): cavedensity[provinces == i] = 1
	cavepoints = place_thing(cavedensity, mindist=128, sites=initcaves, strict=True)
	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_caves(elevation, cavepoints))
	np.random.seed(seed)
	winterqueue.extend(test_place_caves(elevation, cavepoints, winter=True))

	#towns
	print('placing towns')
	towndensity = np.zeros(provinces.shape)
	inittowns = seed_thing(provinces, towndensity, [_swiz(code, 1) for code in biomecodes])
	for i, code in enumerate(biomecodes):
		if _swiz(code, 1): towndensity[provinces == i] = 1
		elif _swiz(code, 0): towndensity[provinces == i] = 0.1
		else: towndensity[provinces == i] = 0.3
	townpoints = place_thing(towndensity, mindist=128, sites=inittowns, strict=True)
	seed = seeds.pop()
	np.random.seed(seed)
	summerqueue.extend(test_place_towns(elevation, townpoints, provinces, biomecodes))
	np.random.seed(seed)
	winterqueue.extend(test_place_towns(elevation, townpoints, provinces, biomecodes))

	#borders
	test_paint_borders(provinces, summercanvas)
	test_paint_borders(provinces, wintercanvas)

	#provinces

	blit_many(summerqueue, summercanvas)
	blit_many(winterqueue, wintercanvas)

	test_paint_centers(centers, summercanvas)
	test_paint_centers(centers, wintercanvas)

	outim = Image.fromarray(np.uint8(summercanvas*255))
	outim.save('{}.png'.format(outprefix))

	outim = Image.fromarray(np.uint8(wintercanvas*255))
	outim.save('{}-winter.png'.format(outprefix))

	provid2domid, provinceborders = compute_domstats(provinces, centers, biomecodes)
	#with open('neighbors.txt') as fh: neighbors = json.load(fh)
	provneighbors, provneighborspec = compute_neighbors(provid2domid, neighbors, hydro['fords'], hydro['bridgefords'], biomecodes)


	with open('{}.map'.format(outprefix), 'w') as fh:
		write_mapfile(fh, outprefix, provinces, biomecodes, provid2domid, provinceborders, provneighbors, provneighborspec)

	#for p1, p2 in provneighbors:
	#	plt.plot([centers[p1][1], centers[p2][1]], [centers[p1][0], centers[p2][0]], lw=0.2)
	plt.imshow(summercanvas)
	plt.imshow(provinces, alpha=0.0)
	x, y = [], []
	for i in range(len(biomecodes)): 
		x.append(centers[i][1])
		y.append(centers[i][0])
		plt.annotate('{}/{}'.format(i, provid2domid[i]), (centers[i][1], centers[i][0]))
	#plt.plot(x, y, lw=0, marke)
	plt.show()
	plt.imshow(wintercanvas)
	plt.imshow(provinces, alpha=0.0)
	plt.show()

def compute_domstats(provinces, centers, biomecodes):

	tfcenters = [tuple(np.array(p) * [-1,1]) for p in centers]
	allcenters = sorted(zip(tfcenters, range(len(centers))))
	allcenters = [np.array(p) for p in allcenters]

	prov2dom = {}
	i = 1
	for p, provid in allcenters:
		#if (0 <= p[0] <= (provinces.shape[0] - 1)) and (0 <= p[1] <= (provinces.shape[1] - 1)):
		if provid < len(biomecodes):
			if (provid % len(biomecodes)) not in prov2dom:
				print(provid, i, p)
				prov2dom[provid % len(biomecodes)] = i
				i += 1

	provinceborders = []
	for r in range(provinces.shape[0]-1, -1, -1):
		y = provinces.shape[0]-1 - r
		last = None
		for c in range(provinces.shape[1]):
			x = c
			current = provinces[r,c]
			if current == last: provinceborders[-1][2] += 1
			else: 
				try: provinceborders.append([x, y, 1, prov2dom[current]])
				except KeyError: 
					print('missing province: {}'.format(current))
					current = None
					continue
			last = current

	return prov2dom, provinceborders

def compute_neighbors(provid2domid, allneighbors, riverneighbors, bridgeneighbors, biomecodes):

	allneighbors = [tuple(sorted(p)) for p in allneighbors]
	riverneighbors = set([tuple(sorted(p)) for p in riverneighbors])
	bridgeneighbors = set([tuple(sorted(p)) for p in bridgeneighbors])
	passneighbors = set()
	impassneighbors = set()
	notneighbors = set()
	roadneighbors = set()

	neighbors = []
	neighborspecs = []
	
	for p1, p2 in allneighbors:
		code1 = biomecodes[p1]
		code2 = biomecodes[p2]

		if _swiz(code1, 22) and _swiz(code2, 22):
			r = np.random.random()
			if r < 0.1: pass
			elif r < 0.3: passneighbors.add((p1,p2))
			else: impassneighbors.add((p1,p2))

		dom1 = provid2domid[p1]
		dom2 = provid2domid[p2]

		pair = tuple(sorted([dom1,dom2]))
		if (p1,p2) in notneighbors: pass
		elif (p1,p2) in impassneighbors:
			neighbors.append(pair)
			neighborspecs.append(pair + (4,))
		elif (p1,p2) in passneighbors:
			neighbors.append(pair)
			neighborspecs.append(pair + (1,))
		elif (p1,p2) in roadneighbors:
			neighbors.append(pair)
			neighborspecs.append(pair + (8,))
		elif (p1,p2) in bridgeneighbors: 
			neighbors.append(pair)
		elif (p1,p2) in riverneighbors: 
			neighbors.append(pair)
			neighborspecs.append(pair + (2,))
		else:
			neighbors.append(pair)
	return neighbors, neighborspecs
			
		

	#	write_mapfile(fh, prefix, provinces, biomecodes, provid2domid, provinceborders, provneighbors, provneighborspec)
def write_mapfile(fh, prefix, provinces, biomecodes, provid2domid, provinceborders, neighbors, neighborspec):
	fh.write('--\n-- Map file for Dominions 5\n--\n-- Illwinter Game Design\n-- www.illwineter.com\n--\n')

	#for k in sorted(provid2domid): print(k, provid2domid[k])

	fh.write('#dom2title {}\n'.format(prefix))
	fh.write('#imagefile {}.png\n'.format(prefix))
	fh.write('#winterimagefile {}-winter.png\n'.format(prefix))
	fh.write('#mapsize {} {}\n'.format(*provinces.shape[::-1]))
	fh.write('#domversion 450\n'.format(*provinces.shape[::-1]))
	fh.write('#maptextcol 0.10 0.10 0.10 1.00\n')
	fh.write('#mapdomcol 255 242 100 25\n')
	fh.write('#description A random map based on Glory of the Gods from Dominions 4\n')

	terr = []
	for provid in range(len(biomecodes)):
		terr.append((provid2domid[provid], biomecodes[provid]))
	#for provid, domid in enumerate(provid2domid): terr.append((domid, biomecodes[provid]))
	for domid, mapcode in sorted(terr): fh.write('#terrain {} {}\n'.format(domid, mapcode))

	for entry in sorted(neighbors + neighborspec):
		if len(entry) == 2: fh.write('#neighbour {} {}\n'.format(*entry))
		elif len(entry) == 3: fh.write('#neighbourspec {} {} {}\n'.format(*entry))

	for pb in provinceborders:
		fh.write('#pb {} {} {} {}\n'.format(*pb))

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-o', required=True, help='Prefix for output files')
	args = parser.parse_args()

	main(outprefix=args.o)

	#plt.imshow(elevation >= 0.25)
	#plt.show()

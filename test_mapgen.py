#!/usr/bin/env python

import mapgen
import mapper
import unittest
import numpy as np

import matplotlib.pyplot as plt

class TestNoisegen(unittest.TestCase):
	width = 128
	height = 64
	shape = (height, width)
	area = height * width
	def test_whitenoise(self):
		empty = mapper.ArrayMap(self.width, self.height)
		#noisy = empty.data + np.random.randint(0, 255, (self.height, self.width), dtype=np.uint8)
		noisy = np.random.random((self.height, self.width))

		#plt.imshow(noisy, vmin=0, vmax=255)
		#plt.show()

		fft = np.fft.fft2(noisy)
		fft[2:-2,:] *= 0
		fft[:,3:-3] *= 0
		fft *= 10
		fft[0,0] = self.area * 0.4
		ifft = np.real(np.fft.ifft2(fft))

		plt.imshow(ifft, vmin=0, vmax=1, cmap=mapper.elevation)
		plt.colorbar()
		plt.show()

if __name__ == '__main__':
	unittest.main()

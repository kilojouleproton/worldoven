import subdivision
import numpy as np
import matplotlib.colors

class ComplexMap(object):
	def __init__(self, width=None, height=None, hwrap=True, vwrap=False):
		self.width = width
		self.height = height
		self.hwrap = hwrap
		self.vwrap = vwrap

class ArrayMap(object):
	def __init__(self, width=None, height=None, data=None, dtype=np.float64):
		self.width = width
		self.height = height
		self.data = np.zeros((height, width), dtype) if data is None else data

	def draw(self, ax, **kwargs):
		return ax.imshow(self.data, **kwargs)

_elevation_data = (
	(0.00, 0.00, 0.50), #deep sea
	(0.00, 0.50, 0.75), #shallow sea
	(0.50, 0.75, 0.25), #lowland
	(0.50, 0.75, 0.25), #lowland
	(0.00, 0.50, 0.00), #midland
	(0.00, 0.50, 0.00), #midland
	(0.50, 0.25, 0.00), #highland
	(0.50, 0.25, 0.00), #highland
)
elevation = matplotlib.colors.ListedColormap(_elevation_data, name='elevation')

#!/usr/bin/env python

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np

class Edge(object):
	def __init__(self, startpt=None, endpt=None):
		self.startpt = startpt
		self.endpt = endpt

	def __truediv__(self, num):
		''' Subdivides by midpoint displacement

		num: Displacement from midpoint as a fraction of the edge's length. Positive values result in counterclockwise displacement
		'''
		if self.startpt is None or self.endpt is None: raise IncompleteEdgeError
		if num == 0: return Edge(self.startpt, self.endpt)

	def displace_midpoint(self, num):
		if self.startpt is None or self.endpt is None: raise IncompleteEdgeError
		if num == 0: return Edge(self.startpt, self.endpt)

		direction = (self.endpt - self.startpt)[::-1]
		if num > 0: direction[0] *= -1
		elif num < 0: direction[1] *= -1

		midpoint = np.mean([self.startpt, self.endpt], axis=0)

		displaced = midpoint + direction * abs(num)
		return [Edge(self.startpt, displaced), Edge(displaced, self.endpt)]

	def cross(self, other):
		return np.cross(self.get_difference(), other.get_difference())

	def dot(self, other):
		return np.dot(self.get_difference(), other.get_difference())

	def draw(self, ax):
		return ax.plot([self.startpt[0], self.endpt[0]], [self.startpt[1], self.endpt[1]])

	def get_difference(self):
		return self.endpt - self.startpt

	def get_length(self): return np.linalg.norm(self.get_difference())

	def __floordiv__(self, num):
		if self.startpt is None or self.endpt is None: raise IncompleteEdgeError
		if num == 1: return Edge(self.startpt, self.endpt)
		elif num == 0: raise ZeroDivisionError
		else: raise NotImplementedError

	def __eq__(self, other):
		if (self.startpt == other.startpt).all() and (self.endpt == other.endpt).all(): return True
		else: return False

	def __str__(self):
		return 'Edge({}, {})'.format(self.startpt, self.endpt)

class IncompleteEdgeError(Exception): pass


class Polygon(object):
	def __init__(self, vertices=None, edges=None):
		if vertices is None and edges is None:
			self.edges = []
			self.vertices = []
		elif vertices is None: 
			self.edges = edges
			self.vertices = self.get_vertices()
		elif edges is None:
			self.vertices = vertices
			self.edges = self.get_edges()
	

	def draw(self, ax, **kwargs):
		vertices = np.array(self.get_vertices(1))
		return ax.plot(vertices[:,0], vertices[:,1], **kwargs)

	def fill(self, ax, **kwargs):
		vertices = np.array(self.get_vertices())
		patch = patches.Polygon(vertices, closed=True, **kwargs)

		return ax.add_patch(patch)
		
	def get_area(self):
		if self.is_convex():
			centroid = self.get_centroid()
			area = 0
			vertices = self.get_vertices()
			for i, vertex in enumerate(vertices):
				area += 0.5 * float(np.cross(vertices[i-1] - centroid, vertices[i] - centroid))
			return float(np.abs(area))

		else: 
			#It looks fine. Not sure about self-intersecting, though
			centroid = self.get_centroid()
			area = 0
			vertices = self.get_vertices()
			for i, vertex in enumerate(vertices):
				area += 0.5 * float(np.cross(vertices[i-1] - centroid, vertices[i] - centroid))
			return float(np.abs(area))

	def get_perimeter(self):
		return sum([edge.get_length() for edge in self.edges])

	def get_centroid(self):
		return np.mean(self.get_vertices(), axis=0)

	def get_vertices(self, fencepost=False):

		if fencepost: return [self.edges[0].startpt] + [edge.endpt for edge in self.edges]
		else: return [edge.endpt for edge in self.edges]

	def get_edges(self): 
		edges = []
		for i, vertex in enumerate(self.vertices):
			if i == len(self.vertices) - 1: edges.append(Edge(self.vertices[i], self.vertices[0]))
			else: edges.append(Edge(self.vertices[i], self.vertices[i+1]))
		return edges

	def is_convex(self):
		last = None
		for i, edge in enumerate(self.edges):
			current = np.sign(self.edges[i-1].dot(self.edges[i]))
			cross = np.sign(self.edges[-1].cross(self.edges[i]))
			#print('d', self.edges[i-1].get_difference(), self.edges[i].get_difference(), current)
			#print('c', self.edges[i-1].get_difference(), self.edges[i].get_difference(), cross)
			if last is None: pass
			elif last <= 0 and current <= 0: pass
			elif last >= 0 and current >= 0: pass
			elif last != current: return False
			last = current
		return True

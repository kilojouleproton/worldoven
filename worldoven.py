#!/usr/bin/env python

import argparse
import mapgen
import json
import mapper
import unittest
import bluenoise
import numpy as np
from astropy.convolution import convolve, kernels
from scipy.ndimage import zoom
import fortune
from PIL import Image

import matplotlib.pyplot as plt

def _swiz(num, place): return (num >> place) % 2

class RasterData(mapper.ArrayMap):
	def __init__(self, width=None, height=None, **kwargs):
		super().__init__(width=width, height=height)

		self.hwrap = kwargs.get('hwrap')
		self.vwrap = kwargs.get('vwrap')
		self.resolution = kwargs.get('resolution', 0.0625)
		self.provinces = kwargs.get('provinces', 150)

	def get_parametric_shape(self, xfactor=1, yfactor=1):
		return int(np.ceil(self.height * self.resolution * yfactor)), int(np.ceil(self.width* self.resolution * xfactor))

	def generate_elevation(self, ruggedness=0.3, hillscraters=1.5, extraisland=0.0, water=0.45, mountains=0.2):
		if water < 0 or water > 1.01: raise ValueError('Invalid water coverage: {}'.format(water))

		xwrap = 2 - self.hwrap
		ywrap = 2 - self.vwrap

		shape = self.get_parametric_shape(xwrap, ywrap)
		trueshape = self.get_parametric_shape()
		area = shape[0] * shape[1]
		truearea = trueshape[0] * trueshape[1]

		noisy = np.random.random(shape)

		#hillnoise = (np.real(self.band_pass(np.random.random(shape), 1, 5)) - 0.5) / 2
		hillnoise = (np.real(self.band_pass(np.random.random(shape), 1, 3)) - 0.5) - 0.001 * hillscraters
		hillnoise *= 3 * hillscraters * 1
		#hillnoise[hillnoise>0] *= 4
		#hillnoise[hillnoise<0] *= 4

		factor = []
		if self.vwrap: factor.append(0.5)
		else: factor.append(1)
		if self.hwrap: factor.append(0.5)
		else: factor.append(1)

		fft = np.fft.fft2(noisy)
		fft *= self.get_blur_mask(ruggedness, shape=shape, factor=factor)
		#fft[3*ywrap:-3*ywrap,:] *= 0.0
		#fft[:,6*xwrap:-6*xwrap] *= 0.0
		#fft[3*ywrap:-3*ywrap,:] *= 0
		#fft[:,6*xwrap:-6*xwrap] *= 0
		fft *= 2
		#fft[0,0] = area * 0.4 #unjustified magic number, probably!
		fft[0,0] = area * 0.375 #unjustified magic number, probably!
		ifft = np.real(np.fft.ifft2(fft))


		if not self.hwrap: 
			ifft = ifft[:,shape[1]//4:-shape[1]//4]
			hillnoise = hillnoise[:,shape[1]//4:-shape[1]//4]
		if not self.vwrap: 
			ifft = ifft[shape[0]//4:-shape[0]//4,:]
			hillnoise = hillnoise[shape[0]//4:-shape[0]//4,:]

		#plt.imshow(ifft, cmap=mapper.elevation, vmin=0, vmax=1)
		#plt.show()

		ifft += hillnoise
		#plt.imshow(ifft, cmap=mapper.elevation, vmin=0, vmax=1)
		#plt.show()

		#adjust initial sea area
		currentwater = np.sum(ifft < 0.25)/truearea
		maxiter = 50
		i = 0
		if not (0.99*water <= currentwater <= 1.01*water):
			for i in range(maxiter):
				if currentwater < 0.5*water: ifft = (ifft + 0.03*negcos(ifft)) / 1.03
				elif currentwater < 0.75*water: ifft = (ifft + 0.01*negcos(ifft)) / 1.01
				elif currentwater < 0.875*water: ifft = (ifft + 0.003*negcos(ifft)) / 1.003
				elif currentwater < 0.999*water: ifft = (ifft + 0.001*negcos(ifft)) / 1.001
				else: break
				currentwater = np.sum(ifft < 0.25)/truearea
		print('water', i, currentwater, water)

		#apply mountains
		currentmountains = np.sum(ifft >= 0.75)/np.sum(ifft >= 0.25)
		i = 0
		if not (0.99*mountains <= currentmountains <= 1.01*mountains):
			for i in range(maxiter):
				if currentmountains < 0.5*mountains: ifft = (ifft + 0.5*sharpm(ifft))/1.5
				elif currentmountains < 0.75*mountains: ifft = (ifft + 0.1*sharpm(ifft))/1.1
				elif currentmountains < 0.999*mountains: ifft = (ifft + 0.05*sharpm(ifft))/1.05
				else: break
				currentmountains = np.sum(ifft >= 0.75)/np.sum(ifft >= 0.25)
		print('mountains', i, currentmountains, mountains)

		print('turbulence', np.mean(hillnoise), np.std(hillnoise))


		#fig = plt.figure()
		#ax = fig.add_subplot(221)
		#ax.imshow(ifft, vmin=0, vmax=1, cmap=mapper.elevation)
		#ax = fig.add_subplot(222)
		#ax.imshow(ifft+hillnoise, vmin=0, vmax=1, cmap=mapper.elevation)
		#ax = fig.add_subplot(223)
		#ax.imshow(hillnoise, vmin=-0.25, vmax=0.25, cmap='RdYlBu')
		#plt.show()

		#plt.imshow(ifft+hillnoise, vmin=0, vmax=1, cmap=mapper.elevation)
		#plt.show()

		return ifft

	def get_blur_mask(self, ruggedness, shape=None, factor=None):
		#0.3 ->
		if shape is None: shape = self.height, self.width
		factor = (1,1) if factor is None else factor

		mask = np.ones(shape)
		##rectangular simplification
		#mask[int(10*factor[0]*ruggedness):-int(10*factor[0]*ruggedness),:] *= 0
		#mask[:,int(10*factor[1]*ruggedness):-int(10*factor[1]*ruggedness)] *= 0
		##fft[3*ywrap:-3*ywrap,:] *= 0.0
		##fft[:,6*xwrap:-6*xwrap] *= 0.0

		vrange = 1
		hrange = 1
		if shape[0] > shape[1]:
			vrange = shape[0] / shape[1]
		elif shape[0] < shape[1]:
			hrange = shape[1] / shape[0]

		ruggedness = (0.3 * ruggedness) ** .5

		k = 4
		#everything outside the bounding box is useless
		mask[int(2*k*vrange*factor[0]*ruggedness):-int(2*k*vrange*factor[0]*ruggedness),:] *= 0
		mask[:,int(2*k*hrange*factor[1]*ruggedness):-int(2*k*hrange*factor[1]*ruggedness)] *= 0

		#some things inside the bounding box are useless
		maxrad = (k*factor[0]*ruggedness)**2 + (k*factor[1]*ruggedness)**2
		maxrad = max(maxrad, 1.)
		print('maxrad', maxrad)
		for r in range(int(2*k*vrange*factor[0]*ruggedness)):
			for c in range(int(2*k*hrange*factor[1]*ruggedness)):
				if (r*hrange)**2 + (c*vrange)**2 > maxrad:
					mask[r,c] *= 0.0
					mask[-r,c] *= 0.0
					mask[-r,-c] *= 0.0
					mask[r,-c] *= 0.0
				elif (r*hrange)**2 + (c*vrange)**2 >= (maxrad - 0.5):
					mask[r,c] *= 0.5
					mask[-r,c] *= 0.5
					mask[-r,-c] *= 0.5
					mask[r,-c] *= 0.5

		mask *= 15 * (shape[0] * shape[1] / 90 / 45)**.5
		#mask /= factor[0] / factor[1]

		return mask

	def band_pass(self, arr, minwavenum, maxwavenum):
		fft = np.fft.fft2(arr)
		hrange = 1
		vrange = 1
		dc = fft[0,0]
		if arr.shape[0] > arr.shape[1]: vrange = arr.shape[0] / arr.shape[1]
		elif arr.shape[0] < arr.shape[1]: hrange = arr.shape[1] / arr.shape[0]

		#cut out everything higher
		fft[int(vrange*maxwavenum):int(-vrange*maxwavenum),:] = 0
		fft[:,int(hrange*maxwavenum):int(-hrange*maxwavenum)] = 0

		fft[:int(vrange*maxwavenum*0.7),:int(hrange*maxwavenum*0.7)] = 0
		fft[:int(vrange*maxwavenum*0.7),int(-hrange*maxwavenum*0.7):] = 0
		fft[int(-vrange*maxwavenum*0.7):,:int(hrange*maxwavenum*0.7)] = 0
		fft[int(-vrange*maxwavenum*0.7):,int(-hrange*maxwavenum*0.7):] = 0

		minrad = minwavenum**2
		maxrad = maxwavenum**2
		for r in range(int(vrange*maxwavenum*0.7), int(vrange*maxwavenum)):
			for c in range(int(hrange*maxwavenum*0.7), int(hrange*maxwavenum)):
				if minrad <= (r*hrange)**2 + (c*vrange)**2 <= maxrad: pass
				else: 
					fft[r,c] = 0
					fft[r,-c] = 0
					fft[-r,c] = 0
					fft[-r,-c] = 0

		fft[0,0] = dc

		ifft = np.fft.ifft2(fft)
		return ifft

	def generate_provinces(self, elevation, seasize=3.5, nprovinces=150, wrap=None):
		wrap = (False, False) if wrap is None else wrap

		provincesize = np.ones(elevation.shape)
		provincesize[elevation < 0.375] = seasize ** 0.5
		provincesize[elevation < 0.25] = seasize 
		provincesize[elevation < 0.125] = seasize ** 2
		#provincesize[(0.3125 <= elevation) * (elevation <= 0.375)] = seasize ** -0.5
		provinces = bluenoise.place_n(provincesize.shape, provincesize, npoints=nprovinces, wrap=(args.vwrap, args.hwrap))
		return provinces

	def raster_voronoi(self, elevation, provinces, seasize=3.5, wrap=None, maxdist=None):
		wrap = (False, False) if wrap is None else wrap
		area = elevation.shape[0] * elevation.shape[1]
		biggest = max(1, seasize**.5)
		if maxdist is None:
			maxdist = 3 * (area/1000000)**.537 * (1/360000*len(provinces))**-0.537
		#mindist = 1 * (area/1000000)**.537 * (1/360000*len(provinces))**-0.537

		#grid = -np.ones((int(elevation.shape[0]//maxdist), int(elevation.shape[1]//maxdist)))
		grid = []
		for r in range(int(np.ceil(elevation.shape[0]/maxdist))):
			grid.append([])
			for c in range(int(np.ceil(elevation.shape[1]/maxdist))):
				grid[-1].append([])
		gridshape = (r+1, c+1)

		for i, p in enumerate(provinces):
			r, c = np.int64(p//maxdist)
			grid[r][c].append(i)

		voronoi = -np.ones(elevation.shape, dtype=np.int64)

		for i, p in enumerate(provinces):
			r, c = np.int64(p)
			voronoi[r,c] = i


		for r in range(elevation.shape[0]):
			gridr = np.int64(r//maxdist)
			for c in range(elevation.shape[1]):
				gridc = np.int64(c//maxdist)
				if voronoi[r,c] != -1: continue
				else: 
					voronoi[r,c] = self._voronoi_get_closest(grid, (r,c), (gridr, gridc), wrap, maxdist, provinces, gridshape)
		
		return voronoi

		plt.imshow(voronoi)
		plt.plot([p[1] for p in provinces], [p[0] for p in provinces], marker='.', color='r', lw=0)
		plt.xticks(np.arange(0, elevation.shape[1], maxdist))
		plt.yticks(np.arange(0, elevation.shape[0], maxdist))
		plt.show()

	def _voronoi_get_closest(self, grid, point, gridpoint, wrap, maxdist, provinces, gridshape):
		distances = {}
		for r in -1, 0, 1:
			adjr = gridpoint[0] + r
			if wrap[0]: adjr = adjr % gridshape[0]
			if not (0 <= adjr < gridshape[0]): continue

			for c in -1, 0, 1:
				adjc = gridpoint[1] + c
				if wrap[1]: adjc = adjc % gridshape[1]
				if not (0 <= adjc < gridshape[1]): continue

				for i in grid[adjr][adjc]:
					distances[i] = (provinces[i][0] - point[0])**2 + (provinces[i][1] - point[1])**2
		mindist = min(distances.values())
		for i in distances: 
			if distances[i] == mindist: return i
		raise ValueError('Extreme outlier!')

	def normalize_provinces(self, elevation, provinces, voronoi, weight=0.6):

		newelev = np.zeros(elevation.shape)
		for r in range(voronoi.shape[0]):
			for c in range(voronoi.shape[1]):
				provid = voronoi[r,c]
				provelev = elevation[tuple(np.int64(provinces[provid]))]
				newelev[r,c] = (1-weight)*elevation[r,c] + weight*provelev
				#newelev[r,c] = provelev


		newelev += (np.random.random(newelev.shape) - 0.5) / 0.5 * 0.025

		kernel = np.ones((3, 3))
		kernel[1,1] += 7
		newelev = convolve(newelev, kernel, boundary='wrap')

		return newelev

		fig = plt.figure()
		fig.set_tight_layout(True)
		ax1 = fig.add_subplot(221)
		ax1.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
		ax3 = fig.add_subplot(223)
		ax3.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
		ax2 = fig.add_subplot(222)
		ax2.imshow(newelev, cmap=mapper.elevation, vmin=0, vmax=1)
		ax4 = fig.add_subplot(224)
		ax4.imshow(newelev, cmap=mapper.elevation, vmin=0, vmax=1)

		from scipy.spatial import Voronoi, voronoi_plot_2d
		vor = Voronoi([p[::-1] for p in provinces])
		voronoi_plot_2d(vor, ax=ax3)
		voronoi_plot_2d(vor, ax=ax4)
		ax3.set_xlim(ax1.get_xlim())
		ax3.set_ylim(ax1.get_ylim()[::-1])
		ax3.invert_yaxis()
		ax4.set_xlim(ax1.get_xlim())
		ax4.set_ylim(ax1.get_ylim()[::-1])
		ax4.invert_yaxis()
		plt.show()

	def get_neighbors(self, provinces, voronoi):
		#only adjacent pixels!
		neighborsets = {}
		for r in range(voronoi.shape[0]):
			for c in range(voronoi.shape[1]):
				thisprov = voronoi[r,c]
				if thisprov not in neighborsets: neighborsets[thisprov] = set()
				neighborsets
				for dr, dc in zip((-1, 1, 0, 0), (0, 0, -1, 1)):
					adjr = r+dr
					adjc = c+dc
					if self.vwrap: adjr = adjr % voronoi.shape[0]
					if not (0 <= adjr < voronoi.shape[0]): continue
					if self.hwrap: adjc = adjc % voronoi.shape[1]
					if not (0 <= adjc < voronoi.shape[1]): continue

					thatprov = voronoi[adjr,adjc]
					if thisprov == thatprov: continue
					neighborsets[thisprov].add(thatprov)

		neighbors = {}
		for here in neighborsets:
			nset = neighborsets[here]
			aroundme = []
			for there in nset:
				vec = provinces[there] - provinces[here]

				if self.vwrap and (vec[0] > (0.5 * voronoi.shape[0])): vec[0] -= voronoi.shape[0]
				if self.hwrap and (vec[1] > (0.5 * voronoi.shape[1])): vec[1] -= voronoi.shape[1]

				arctan = np.arctan(vec[0]/vec[1])
				if vec[1] < 0: arctan += np.pi 
				aroundme.append((arctan % (2*np.pi), there))

			neighbors[here] = [x[1] for x in sorted(aroundme)]

		prov2ridges = {}
		for prov1 in sorted(neighborsets):
			if prov1 not in prov2ridges: prov2ridges[prov1] = {}
			for prov2 in neighborsets[prov1]:
				for prov3 in neighborsets[prov1].intersection(neighborsets[prov2]):
					if prov2 not in prov2ridges[prov1]: prov2ridges[prov1][prov2] = {}
					rawrect = np.array([provinces[prov1], provinces[prov2], provinces[prov3]])

					#row mean
					rowangles = (rawrect[:,0]/voronoi.shape[0]*2*np.pi)
					rowmean = np.arctan2(np.mean(np.sin(rowangles)), np.mean(np.cos(rowangles))) /2/np.pi*voronoi.shape[0]

					#column mean
					colangles = (rawrect[:,1]/voronoi.shape[1]*2*np.pi)
					colmean = np.arctan2(np.mean(np.sin(colangles)), np.mean(np.cos(colangles))) /2/np.pi*voronoi.shape[1]

					ridge = np.mean([provinces[prov1], provinces[prov2], provinces[prov3]], axis=0)

					ridge = np.array([rowmean % voronoi.shape[0], colmean % voronoi.shape[1]]) 

					prov2ridges[prov1][prov2][prov3] = ridge

		return neighbors, prov2ridges

	def generate_biomes(self, elevation, provinces, voronoi, neighbors):

		land = []
		sea = []
		mapcodes = [0 for p in provinces]

		for i, p in enumerate(provinces):
			if elevation[tuple(np.int64(p))] >= 0.25: land.append(i)
			else: sea.append(i)

		#place swamps
		hydration = []
		for provid in land:	
			h = np.sum((voronoi == provid) * (elevation < 0.25)) / np.sum(voronoi == provid)
			hydration.append((h, provid))
		hydration.sort()
		for i in range(int(np.ceil(0.02 * len(land)))):
			mapcodes[hydration[-1-i][1]] += 2**5
			mapcodes[provid] += 2**19

		#place highlands
		highelev = []
		midelev = []
		lowelev = []
		for i, p in enumerate(provinces):
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev >= 0.75 or meanelev < 0.125: highelev.append(i)
			elif meanelev >= 0.5: midelev.append(i)
			elif meanelev >= 0.25: lowelev.append(i)

		np.random.shuffle(highelev)
		np.random.shuffle(midelev)
		np.random.shuffle(lowelev)
		highqueue = highelev + midelev + lowelev
		highlandquota = np.ceil(0.1 * len(provinces))
		while highlandquota and highqueue:
			provid = highqueue.pop(0)
			if mapcodes[provid]: continue
			highlandquota -= 1
			mapcodes[provid] += 2**4

		#place forests
		forestquota = np.ceil(0.25 * len(provinces))
		while forestquota and highqueue:
			provid = highqueue.pop(0)
			if mapcodes[provid]: continue
			forestquota -= 1
			mapcodes[provid] += 2**7
			if np.random.random() > 0.75:
				mapcodes[provid] += 2**19

		#caves
		nonlowelev = []
		lowelev = []
		for i in land:
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev >= 0.5: nonlowelev.append(i)
			elif meanelev >= 0.25: lowelev.append(i)

		np.random.shuffle(nonlowelev)
		np.random.shuffle(lowelev)
			
		cavequeue = nonlowelev + lowelev
		cavequota = np.ceil(0.05 * len(provinces))
		np.random.shuffle(cavequeue)
		while cavequeue and cavequota:
			provid = cavequeue.pop(0)
			if mapcodes[provid]: continue
			cavequota -= 1
			mapcodes[provid] += 2**12

		#place wastelands
		midelev = []
		lowelev = []
		highelev = []
		for i, p in enumerate(provinces):
			if i in sea: continue
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev >= 0.75: highelev.append(i)
			elif meanelev >= 0.5: midelev.append(i)
			elif meanelev >= 0.25: lowelev.append(i)

		np.random.shuffle(highelev)
		np.random.shuffle(midelev)
		np.random.shuffle(lowelev)
		wastequeue = midelev + lowelev + highelev

		wastequota = np.ceil(0.1 * len(provinces))
		while wastequota and wastequeue:
			provid = wastequeue.pop(0)
			if mapcodes[provid]: continue
			wastequota -= 1
			mapcodes[provid] += 2**6

		#place farms
		nonhighelev = []
		highelev = []
		for i, p in enumerate(provinces):
			if i in sea: continue
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev >= 0.75: highelev.append(i)
			elif meanelev >= 0.25: nonhighelev.append(i)

		np.random.shuffle(nonhighelev)
		np.random.shuffle(highelev)
		farmqueue = nonhighelev + highelev

		farmquota = np.ceil(0.1 * len(provinces))
		while farmquota and farmqueue:
			provid = farmqueue.pop(0)
			if mapcodes[provid]: continue
			farmquota -= 1
			mapcodes[provid] += 2**8


		#deep sea
		for i, p in enumerate(provinces):
			if i in land: continue
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev < 0.125: mapcodes[i] += 2**11

		#land/sea
		for i in sea: mapcodes[i] += 2**2

		#elemental sites: F/A/W/E
		for i in range(np.random.poisson(0.0034*len(land))): mapcodes[np.random.choice(land)] += 2**13
		for i in range(np.random.poisson(0.0034*len(land))): mapcodes[np.random.choice(land)] += 2**14
		for i in range(np.random.poisson(0.0034*len(land))): 
			provid = np.random.choice(land)
			if not _swiz(mapcodes[provid], 16): mapcodes[provid] += 2**16
		for i in sea: mapcodes[i] += 2**15

		
		#freshwater lakes
		for this in sea:
			landlocked = True
			for that in neighbors[this]:
				if that in sea: 
					landlocked = False
					break
			if landlocked: mapcodes[this] += 2**3

		mountains = []
		#mountains
		for i in land:
			maxelev = np.max(elevation[voronoi == i])
			if maxelev >= 0.8125: 
				mapcodes[i] += 2**22
				mountains.append(i)

		smallquota = np.ceil(0.5 * len(provinces))
		largequota = np.ceil(0.1 * len(provinces))
		sizequeue = list(range(len(provinces)))
		np.random.shuffle(sizequeue)
		while (smallquota or largequota) and sizequeue:
			provid = sizequeue.pop(0)
			if smallquota:
				smallquota -= 1
				mapcodes[provid] += 2**0
			elif largequota:
				largequota -= 1
				mapcodes[provid] += 2**1

		return mapcodes

		#plt.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
		#plt.plot([p[1] for p in provinces], [p[0] for p in provinces], lw=0, marker='.', color='r')
		#for i, x in enumerate(mapcodes):
		#	plt.annotate('{}:{}'.format(i, x), provinces[i][::-1])
		#plt.show()

	def generate_rivers(self, elevation, provinces, voronoi, neighbors, ridges, nrivers=None, meanlength=None, maxlen=100, biomes=None):
		#nrivers = np.random.poisson(4) if nrivers is None else nrivers
		nrivers = np.random.poisson(20) if nrivers is None else nrivers
		meanlength = 5 if meanlength is None else meanlength

		land = set()
		sea = set()
		for i, p in enumerate(provinces):
			meanelev = np.mean(elevation[voronoi == i])
			if meanelev < 0.25: sea.add(i)
			else: land.add(i)

		mouthqueue = []
		talusqueue = []
		inlandqueue = []

		seaborders = {}
		searidges = set()
		for p1 in ridges:
			for p2 in ridges[p1]:
				for p3 in ridges[p1][p2]:
					w = 0
					l = 0
					if p1 in sea: w += 1
					else: l += 1
					if p2 in sea: w += 1
					else: l += 1
					if p3 in sea: w += 1
					else: l += 1

					if w > 0 and l > 0: mouthqueue.append((p1,p2,p3))
					if w > 0 and l == 0: talusqueue.append((p1,p2,p3))
					if w <= 1: inlandqueue.append((p1,p2,p3))

					ridge = ridges[p1][p2][p3]
					ridgeelev = elevation[int(ridge[0]), int(ridge[1])]
					if ridgeelev < 0.25: searidges.add(tuple(sorted((p1,p2,p3))))

					try: seaborders[w].append(ridges[p1][p2][p3])
					except KeyError: seaborders[w] = [ridges[p1][p2][p3]]

		neighborsets = {}
		for i in neighbors: neighborsets[i] = set(neighbors[i])

		np.random.shuffle(mouthqueue)

		ridgeban = set()
		rivers = []
		riverquota = nrivers
		fords = []
		while riverquota and mouthqueue:
			#river initiation
			river = []
			ridge = mouthqueue.pop(0)

			p1, p2, p3 = ridge
			if tuple(sorted([p1,p2,p3])) in ridgeban: continue
			ridgeban.add(tuple(sorted([p1,p2,p3])))
			#if set(ridge).intersection(flowban): continue
			#flowban = flowban.union(set(ridge))

			#flow it down to the sea
			foundmouth = False
			for j in ridge: 
				for k in ridge:
					if j == k: continue
					if k in sea:
						for choice in neighborsets[j].intersection(neighborsets[k]):
							if tuple(sorted([j,k,choice])) not in ridgeban:
								mouth = (j, k, choice)
								if mouth in talusqueue:
									river.append(mouth)
									foundmouth = True
									ridgeban.add(tuple(sorted([j,k,choice])))
									break
						if foundmouth: break
					if foundmouth: break
				if foundmouth: break
			if not foundmouth: continue
			#river.append(ridge)


			#flow it up to the source
			flowing = True
			prov1, prov2, prov3 = ridge
			prov4, prov5, prov6 = None, None, None
			while flowing:
				if len(river) > maxlen: break

				nextridge = self._flow_uphill(elevation, neighborsets, ridges, (prov1, prov2, prov3), sea, ridgeban, inlandqueue, searidges)
				if nextridge is None: #not a river after all!
					prov4 = None
					prov5 = None
					prov6 = None
					flowing = False
					break
				elif tuple(sorted(nextridge)) in ridgeban: 
					prov4 = None
					prov5 = None
					prov6 = None
					flowing = False
					break
				else:
					prov4, prov5, prov6 = nextridge

					ford = set((prov1,prov2,prov3)).intersection(set((prov4,prov5,prov6)))
					if len(ford) >= 2: 
						for bank1 in ford:
							for bank2 in ford:
								if bank1 < bank2: fords.append(tuple(ford))

					river.append((prov1,prov2,prov3))
					prov1 = prov4
					prov2 = prov5
					prov3 = prov6
					ridgeban.add(tuple(sorted((prov1,prov2,prov3))))

			if prov4 is not None:
				rivers.append((prov4,prov5,prov6))

			if len(river) < 3: 
				#ridgeban = set()
				continue

			riverquota -= 1
			rivers.append(river)


		return rivers, fords
		print('rivers', len(rivers))
		for river in rivers:
			x = []
			y = []
			for p1,p2,p3 in river:
				ridge = ridges[p1][p2][p3]
				x.append(ridge[1])
				y.append(ridge[0])
			plt.plot(x, y, marker='.', color='c', markersize=20, lw=2)

		for p1 in ridges:
			for p2 in ridges[p1]:
				for p3 in ridges[p1][p2]:
					if p1 <= p2 <= p3:
						pass
						#plt.annotate('{}:{}:{}'.format(p1,p2,p3), ridges[p1][p2][p3][::-1])
						#ridge = ridges[p1][p2][p3]
						#ridgeelev = elevation[int(ridge[0]), int(ridge[1])]

						#plt.annotate('{}:{}:{},{:0.2f}'.format(p1,p2,p3, ridgeelev), ridges[p1][p2][p3][::-1])
		#for p1, p2, p3 in searidges:
		#	ridge = ridges[p1][p2][p3]
		#	plt.plot(ridge[1], ridge[0], marker='+', color='r', markersize=20)

		#plt.plot([p[1] for p in seaborders[0]], [p[0] for p in seaborders[0]], lw=0, marker='.', markersize=5, color='r')
		#plt.plot([p[1] for p in seaborders[1]], [p[0] for p in seaborders[1]], lw=0, marker='.', markersize=5, color='orange')
		#plt.plot([p[1] for p in seaborders[2]], [p[0] for p in seaborders[2]], lw=0, marker='.', markersize=5, color='y')
		#plt.plot([p[1] for p in seaborders[3]], [p[0] for p in seaborders[3]], lw=0, marker='.', markersize=5, color='c')

		for i, p in enumerate(provinces): 
			plt.plot(p[1], p[0], lw=0, marker='x', markersize=20, color='k')
			plt.annotate('{}:{}'.format(i, biomes[i]), p[::-1])

		plt.imshow(voronoi)
		plt.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1, alpha=0.5)
		print('fords', fords)
		for river in rivers: print('river', river)
		plt.show()

	def _flow_uphill(self, elevation, neighborsets, ridges, triplet, sea, ridgeban, inlandqueue, searidges):
		prov1, prov2, prov3 = triplet
		startpoint = ridges[prov1][prov2][prov3]
		startelev = elevation[int(startpoint[0]*self.resolution), int(startpoint[1]*self.resolution)]
		startset = set(triplet)

		nextridge = []
		#1-2 ridges
		options = neighborsets[prov1].intersection(neighborsets[prov2]) - startset
		for option in options:
			ridge = ridges[prov1][prov2][option]
			if tuple(sorted((ridge))) in ridgeban: continue
			if tuple(sorted((ridge))) in searidges: continue
			ridgeelev = elevation[int(ridge[0]), int(ridge[1])]
			if ridgeelev < startelev: continue
			#if (prov1,prov2,option) not in inlandqueue: continue
			nextridge.append((ridgeelev, prov1,prov2,option))

		#2-3 ridges
		options = neighborsets[prov2].intersection(neighborsets[prov3]) - startset
		for option in options:
			ridge = ridges[prov2][prov3][option]
			if tuple(sorted((ridge))) in ridgeban: continue
			if tuple(sorted((ridge))) in searidges: continue
			ridgeelev = elevation[int(ridge[0]), int(ridge[1])]
			if ridgeelev < startelev: continue
			#if (prov3,prov1,option) not in inlandqueue: continue
			nextridge.append((ridgeelev, prov2,prov3,option))

		#3-1 ridges
		options = neighborsets[prov3].intersection(neighborsets[prov1]) - startset
		for option in options:
			ridge = ridges[prov3][prov1][option]
			if tuple(sorted((ridge))) in ridgeban: continue
			if tuple(sorted((ridge))) in searidges: continue
			ridgeelev = elevation[int(ridge[0]), int(ridge[1])]
			if ridgeelev < startelev: continue
			#if (prov3,prov1,option) not in inlandqueue: continue
			nextridge.append((ridgeelev, prov3,prov1,option))

		if not nextridge:
			for prov in triplet:
				for neighbor in neighborsets[prov]:
					if neighbor not in sea: 
						for option in neighborsets[prov].intersection(neighborsets[neighbor]):
							if option in sea: continue
							ridge = ridges[prov][neighbor][option]
							if tuple(sorted(ridge)) in ridgeban: continue
							if tuple(sorted((ridge))) in searidges: continue
							ridgeelev = elevation[int(ridge[0]), int(ridge[1])]
							if ridgeelev < startelev: continue
							nextridge.append((ridgeelev, prov,neighbor,option))

		nextridge.sort()
		if nextridge: return nextridge[-1][1:]
		else: return None

	def plant_forests(self, elevation, provinces, voronoi, neighbors, biomes):
		foresttype = -np.ones(voronoi.shape, dtype=np.int64)
		forestdensity = -np.ones(voronoi.shape)

		land = np.zeros(voronoi.shape, dtype=np.uint8)
		waste = np.zeros(voronoi.shape, dtype=np.uint8)

		for i in range(len(provinces)):
			biome = biomes[i]
			meanelev = np.mean(elevation[voronoi == i])
			neighborbiomes = [biomes[j] for j in neighbors[i]]

		
			if meanelev > 0.25: 
				land[voronoi == i] = 1
				foresttype[voronoi == i] = 5

			if _swiz(biome, 7):
				nearwaste = False
				nearwarmer = False
				nearcolder = False
				nearswamp = False
				for nb in neighborbiomes:
					if _swiz(nb, 6): nearwaste = True
					if _swiz(nb, 29): nearwarmer = True
					if _swiz(nb, 30): nearcolder = True
					if _swiz(nb, 5): nearswamp = True
				if _swiz(biome, 2) and not _swiz(biome, 11): foresttype[voronoi == i] = 11
				elif meanelev > 0.25:
					if meanelev > 0.75: foresttype[voronoi == i] = 1
					elif nearwaste: foresttype[voronoi == i] = 7
					elif nearswamp: foresttype[voronoi == i] = 9
					else: foresttype[voronoi == i] = np.random.choice([3, 5])

					if nearwarmer: foresttype[voronoi == i] = np.clip(foresttype[voronoi == i] + 2, 1, 9) 
					if nearcolder: foresttype[voronoi == i] = np.clip(foresttype[voronoi == i] - 2, 1, 9)
			else: 
				if _swiz(biome, 6): waste[voronoi == i] = 1
				foresttype[voronoi == i] = 0

		forestdensity = np.zeros(voronoi.shape)
		forestdensity += foresttype > 0
		#forestdensity *= np.clip(np.random.chisquare(3, voronoi.shape), 0, None)
		forestdensity *= np.clip(np.random.normal(0.8, 0.3, voronoi.shape), 0, None)
		forestdensity += np.random.random(voronoi.shape) * (foresttype == 0) * 0.5 * (waste == 0) * (land == 1)
		forestdensity += np.random.random(voronoi.shape) * (foresttype == 0) * 0.2 * (waste == 1) * (land == 1)
		#forestdensity += np.random.random(voronoi.shape) * (foresttype != 0) * 0.1

		if self.hwrap or self.vwrap: boundary = 'wrap'
		else: boundary = 'extend'
		kernel = np.ones((5,5))
		kernel[1:-1,1:-1] = 2
		kernel[2,2] = 4

		landforest = forestdensity * land
		seaforest = forestdensity * (1-land)

		landforest = convolve(landforest, kernel, boundary=boundary)
		seaforest = convolve(seaforest, kernel, boundary=boundary)

		forestdensity = np.clip(landforest + seaforest, 0, 1)

		return np.uint8(foresttype), forestdensity

		#none: 0
		#boreal: 1
		#mixed: 3
		#deciduous: 5
		#savanna: 7
		#tropical: 9
		#kelp: 11
		plt.imshow(forestmindist)
		plt.show()

	def upscale_dither(self, small, ditherweight=0.05, radius=2.5, order=3):
		big = zoom(small, (self.height/small.shape[0],self.width/small.shape[1]), order=order)

		if ditherweight:
			dither = (np.random.random(big.shape) - 0.5) / 0.5 * ditherweight
			big += dither

		if radius:
			if (self.hwrap or self.vwrap): boundary = 'wrap'
			else: boundary = 'extend'
			kernel = kernels.Gaussian2DKernel(x_stddev=radius, y_stddev=radius, x_size=9, y_size=9)
			big = convolve(big, kernel, boundary=boundary)

		return big

		fig = plt.figure()
		#ax1 = fig.add_subplot(121)
		#ax1.imshow(small)
		ax2 = fig.add_subplot(111)
		ax2.imshow(big, cmap=mapper.elevation, vmin=0, vmax=1)
		plt.show()

	def get_maxdist(self, provinces):
		dist = None
		def _dist(p1, p2):
			guess = 0
			#(p1[0]-p2[0])**2 + 
			if self.hwrap:
				guess += min((p1[1]-p2[1])**2, (p1[1]-p2[1]+self.width)**2, (p1[1]-p2[1]-self.width)**2)
			else: guess += (p1[1]-p2[1])**2 
			if self.vwrap:
				guess += min((p1[0]-p2[0])**2, (p1[0]-p2[0]+self.height)**2, (p1[0]-p2[0]-self.height)**2)
			else: guess += (p1[0]-p2[0])**2 
			return guess

		for i, prov1 in enumerate(provinces):
			for j, prov2 in enumerate(provinces):
				if i > j: continue
				elif dist is None: dist = _dist(prov1, prov2)
				else: dist = max(dist, _dist(prov1, prov2))
		return np.sqrt(dist)

	def spiral_voronoi(self, shape, provinces):
		voronoi = -np.ones(shape)

		intprov = [np.int64(p) for p in provinces]

		radius = 0
		painting = True
		while painting:
			painting = False
			if radius == 0:
				for i, p in enumerate(intprov): 
					painting = True
					voronoi[tuple(p)] = i
			else:
				dr, dc = 0, 0
				r, c = -radius, -radius
				for dc in range(radius*2+1):
					for i, p in enumerate(intprov): 
						row = p[0]+r+dr
						if self.vwrap: row = row % (voronoi.shape[0] - 1)
						if not (0 <= row < voronoi.shape[0]): continue
						col = p[1]+c+dc
						if self.hwrap: col = col % (voronoi.shape[1] - 1)
						if not (0 <= col < voronoi.shape[1]): continue
						if voronoi[row,col] == -1: 
							voronoi[row,col] = i
							painting = True
				for dr in range(radius*2+1):
					for i, p in enumerate(intprov): 
						row = p[0]+r+dr
						if self.vwrap: row = row % (voronoi.shape[0] - 1)
						if not (0 <= row < voronoi.shape[0]): continue
						col = p[1]+c+dc
						if self.hwrap: col = col % (voronoi.shape[1] - 1)
						if not (0 <= col < voronoi.shape[1]): continue
						if voronoi[row,col] == -1: 
							voronoi[row,col] = i
							painting = True
				dr, dc = 0, 0
				for dr in range(radius*2+1):
					for i, p in enumerate(intprov): 
						row = p[0]+r+dr
						if self.vwrap: row = row % (voronoi.shape[0] - 1)
						if not (0 <= row < voronoi.shape[0]): continue
						col = p[1]+c+dc
						if self.hwrap: col = col % (voronoi.shape[1] - 1)
						if not (0 <= col < voronoi.shape[1]): continue
						if voronoi[row,col] == -1: 
							voronoi[row,col] = i
							painting = True
				for dc in range(radius*2+1):
					for i, p in enumerate(intprov): 
						row = p[0]+r+dr
						if self.vwrap: row = row % (voronoi.shape[0] - 1)
						if not (0 <= row < voronoi.shape[0]): continue
						col = p[1]+c+dc
						if self.hwrap: col = col % (voronoi.shape[1] - 1)
						if not (0 <= col < voronoi.shape[1]): continue
						if voronoi[row,col] == -1: 
							voronoi[row,col] = i
							painting = True
			radius += 1
			if radius == 256: break
		
		return voronoi

	def vflow(self, elevation, vor, provinces, wrap=None, nrivers=None):
		wrap = (0,0) if wrap is None else wrap
		nrivers = np.random.poisson(10) if nrivers is None else nrivers



		validverts = set()
		landverts = set()
		seaverts = set()
		highverts = set()
		midverts = set()
		lowverts = set()

		pointelevs = []
		for p in vor.points:
			if wrap[0]: y = p[0] % elevation.shape[0]
			else: y = -p[0] % elevation.shape[0]

			if wrap[1]: x = p[1] % elevation.shape[1]
			else: x = -p[1] % elevation.shape[1]
			pointelevs.append(elevation[int(y), int(x)])

		truepos = []
		vertexelevation = []
		for i, p in enumerate(vor.vertices):
			#plt.scatter(p[1], p[0]) 
			if not ((0 <= p[1] < elevation.shape[1]) and (0 <= p[0] < elevation.shape[0])): 

				if wrap[0]: y = p[0] % elevation.shape[0]
				else: y = -p[0] % elevation.shape[0]

				if wrap[1]: x = p[1] % elevation.shape[1]
				else: x = -p[1] % elevation.shape[1]
			else: 
				y = p[0]
				x = p[1]
				validverts.add(i)
			y = np.clip(y, 0, elevation.shape[0]-1)
			x = np.clip(x, 0, elevation.shape[1]-1)
			truepos.append(np.array((y, x)))
			elev = elevation[int(y), int(x)]

			if elev < 0.25: seaverts.add(i)
			else: 
				if elev < 0.5: lowverts.add(i)
				elif elev < 0.75: midverts.add(i)
				else: highverts.add(i)
				landverts.add(i)
			vertexelevation.append(elev)

		neighbors = {}
		validneighbors = {}
		for i in range(len(vor.vertices)): neighbors[i] = set()
		for v1, v2 in vor.ridge_vertices:
			try: neighbors[v1].add(v2)
			except KeyError: pass
			try: neighbors[v2].add(v1)
			except KeyError: pass

		sourcequeue = list(validverts.intersection(highverts.union(midverts)))
		np.random.shuffle(sourcequeue)

		inriver = set()
		flowban = set()
		rivers = []
		riverquota = nrivers
		maxattempts = 20
		initlength = len(sourcequeue)
		lakes = set()
		fords = set()
		maxlength = 20
		while riverquota and sourcequeue:
			#if  len(sourcequeue) < (initlength - maxattempts * nrivers): break

			river = []
			river.append(sourcequeue.pop(0))

			flowing = True
			forbidden = False
			lake = False
			length = 0
			while flowing:
				length += 1
				if length > maxlength: break
				flowing = False

				thiselev = vertexelevation[river[-1]]
				choices = []
				for i in neighbors[river[-1]]:
					if i in river: continue
					if vertexelevation[i] < thiselev: 
						if vertexelevation[i] >= 0.25:
							y, x = np.mean([vor.vertices[i], vor.vertices[river[-1]]], axis=0)
							if wrap[0]: y = y % elevation.shape[0]
							else: y = -y % elevation.shape[0]
							if wrap[1]: x = x % elevation.shape[1]
							else: x = -x % elevation.shape[1]
							y = np.clip(y, 0, elevation.shape[0]-1)
							x = np.clip(x, 0, elevation.shape[1]-1)
							mpelev = elevation[int(y), int(x)]
							if mpelev < 0.25: continue
						choices.append((vertexelevation[i], i))
				if not choices: 
					lake = True
					for i in neighbors[river[-1]]:
						if vertexelevation[i] < (thiselev + 0.1): choices.append((vertexelevation[i], i))
					
					if not choices: break

				nextvertex = sorted(choices)[0][1]

				if nextvertex in flowban: 
					forbidden = True
					break
				elif nextvertex in inriver: break
				elif nextvertex in seaverts: 
					river.append(nextvertex)
					flowing = False
					break
				else: 
					river.append(nextvertex)
					flowing = True

			if len(river) < 3: continue
			if forbidden: continue
			if lake: 
				if river[-1] not in seaverts: lakes.add(river[-1])

			for i in range(len(river)-1):
				pair = river[i:i+2]
				try: ridgeid = vor.ridge_vertices.index(pair)
				except ValueError: ridgeid = vor.ridge_vertices.index(pair[::-1])

				fords.add(ridgeid)

			rivers.append(river)
			riverquota -= 1
		bridges = []
		bridgequeue = list(fords)
		bridgequota = int(np.ceil(0.1*len(fords)))
		np.random.shuffle(bridgequeue)
		while bridgequota and bridgequeue:
			bridgeid = bridgequeue.pop(0)
			pt1, pt2 = vor.ridge_points[bridgeid]

			if pointelevs[pt1] < 0.25 or pointelevs[pt2] < 0.25: continue

			bridgequota -= 1
			bridges.append(bridgeid)

		outrivers = []
		outfords = []
		outbridges = []
		outbridgefords = []
		for river in rivers:
			outriver = []
			for ridge in river: outriver.append(truepos[ridge])
			outrivers.append(outriver)

		for ford in fords:
			v1, v2 = vor.ridge_vertices[ford]
			v1 = truepos[vor.ridge_vertices[ford][0]]
			v2 = truepos[vor.ridge_vertices[ford][1]]

			if ford in bridges:
				pt1, pt2 = vor.ridge_points[ford]
				outbridges.append((truepos[pt1 % len(provinces)], truepos[pt2 % len(provinces)]))
				outbridgefords.append((pt1 % len(provinces), pt2 % len(provinces)))
			else:
				pt1, pt2 = vor.ridge_points[ford]
				outfords.append((pt1 % len(provinces), pt2 % len(provinces)))
			


		fig = plt.figure()
		fig.set_tight_layout(1)
		fig.set_figwidth(10)
		fig.set_figheight(5)
		ax = fig.gca()

		ax.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
		for v1, v2 in vor.ridge_vertices:
			if v1 == -1 or v2 == -1: continue
			x = [vor.vertices[v1][1], vor.vertices[v2][1]]
			y = [vor.vertices[v1][0], vor.vertices[v2][0]]
			ax.plot(x, y, color='gray')

		for river in rivers:
			data = np.vstack([vor.vertices[i] for i in river])
			#data = np.vstack([truepos[i] for i in river])
			ax.plot(data[:,1], data[:,0], marker='.', color='cyan', lw=4)
		for lakeid in lakes:
			pt = truepos[lakeid]
			ax.plot(pt[1], pt[0], lw=0, marker='.', markersize=50, color='cyan')

		for bridge in bridges:
			v1, v2 = vor.ridge_vertices[bridge]
			p = np.mean([vor.vertices[v1], vor.vertices[v2]], axis=0)
			print(p)
			ax.plot(p[1], p[0], lw=0, markersize=20, color='brown', marker='.')
		#ax.plot([vor.vertices[i][1] for i in seaverts], [vor.vertices[i][0] for i in seaverts], lw=0, marker='.', color='tab:blue')
		#ax.plot([vor.vertices[i][1] for i in landverts], [vor.vertices[i][0] for i in landverts], lw=0, marker='.', color='tab:orange')
		ax.plot([p[1] for p in vor.points], [p[0] for p in vor.points], marker='p', lw=0, color='red')
		ax.set_xlim([0, elevation.shape[1]])
		ax.set_ylim([0, elevation.shape[0]])
		fig.savefig('tileme.png')
		plt.show()
		return outrivers, outfords, outbridges, outbridgefords
		

def negcos(x): return np.clip(-np.cos(x*np.pi/2), 0, 1)

def sharpm(x):
	return np.clip(x*(x<0.25) + 2*x*(x>=0.25) - 0.25*(x>=0.25), 0, 1)


def main(args):
	if args.seed: np.random.seed(args.seed)

	rasterdata = RasterData(width=args.width, height=args.height, hwrap=args.hwrap, vwrap=args.vwrap, resolution=args.resolution)
	elevation = rasterdata.generate_elevation(mountains=args.mountains, ruggedness=args.ruggedness, hillscraters=args.hillscraters, extraisland=args.extraisland, water=args.water)
	provinces = [1]
	while len(provinces) <= 2:
		provinces = rasterdata.generate_provinces(elevation, seasize=args.seasize, nprovinces=args.provinces, wrap=(args.vwrap, args.hwrap))
	print('provinces', len(provinces))
	maxdist = rasterdata.get_maxdist(provinces)
	print('maxdist', maxdist)

	voronoi = rasterdata.raster_voronoi(elevation, provinces, maxdist=maxdist*np.sqrt(rasterdata.resolution))
	neighbors, ridges = rasterdata.get_neighbors(provinces, voronoi)
	edgelist = []
	for here in neighbors:
		for there in neighbors[here]:
			edgelist.append(sorted([int(here), int(there)]))
	with open('neighbors.txt', 'w') as fh: json.dump(edgelist, fh)

	#for prov in neighbors:
	#	print(prov, neighbors[prov])
	normelev = rasterdata.normalize_provinces(elevation, provinces, voronoi)

	biomes = rasterdata.generate_biomes(normelev, provinces, voronoi, neighbors)
	with open('biomes.txt', 'w') as fh: json.dump(biomes, fh)

	for p1 in sorted(ridges):
		for p2 in sorted(ridges[p1]):
			for p3 in sorted(ridges[p1][p2]):
				pass#print(p1, p2, p3, ridges[p1][p2][p3])
	rivers, fords = rasterdata.generate_rivers(normelev, provinces, voronoi, neighbors, ridges, biomes=biomes)
	print('fords', fords)

	foresttype, forestdensity = rasterdata.plant_forests(normelev, provinces, voronoi, neighbors, biomes)
	im = Image.fromarray(foresttype)
	im.save('foresttype.png')
	im = Image.fromarray(np.uint8(255*forestdensity))
	im.save('forestdensity.png')

	bigelev = rasterdata.upscale_dither(normelev, ditherweight=0.05, radius=2.5, order=1)
	im = Image.fromarray(np.uint8(255*np.clip(bigelev, 0, 1)))
	im.save('elevation.png')
	bigprovinces = []
	for p in provinces:
		bigprovinces.append(np.array([np.clip(p[0]/rasterdata.resolution, 0, rasterdata.height-1), np.clip(p[1]/rasterdata.resolution, 0, rasterdata.width-1)]))
	#bigvoronoi = rasterdata.raster_voronoi(bigelev, bigprovinces, maxdist=2*maxdist)
	#bigvoronoi = rasterdata.upscale_dither(voronoi, ditherweight=0.0, radius=0.0, order=0)
	bigvoronoi, vor = fortune.scipykludge(bigelev.shape, bigprovinces, wrap=(rasterdata.vwrap, rasterdata.hwrap))
	with open('centers.txt', 'w') as fh: json.dump([tuple(x) for x in vor.points], fh)

	im = Image.fromarray(np.uint16(bigvoronoi))
	im.save('provinces.png')

	rivers, fords, bridges, bridgefords = rasterdata.vflow(bigelev, vor, provinces=provinces, wrap=(rasterdata.vwrap, rasterdata.hwrap))
	outrivers = []
	for river in rivers: outrivers.append([list(x) for x in river])
	outfords = []
	for ford in fords: outfords.append([int(x) for x in ford])
	outbridges = []
	for bridge in bridges: outbridges.append([list(x) for x in bridge])
	outbridgefords = []
	for bridgeford in bridgefords: outbridgefords.append([int(x) for x in bridgeford])
	with open('rivers.txt', 'w') as fh: json.dump({'rivers':outrivers, 'fords':outfords, 'bridges':outbridges, 'bridgefords':outbridgefords}, fh)

	#for some reason, this is bad bad bad and produces lots of extra edges
	#provpairs = set()
	#for p1, p2 in vor.ridge_points: provpairs.add((int(p1 % len(provinces)), int(p2 % len(provinces))))
	#with open('neighbors.txt', 'w') as fh: json.dump(list(provpairs), fh)

	#with open('rivers.txt', 'w') as fh: json.dump({'rivers':outrivers}, fh)
	#with open('rivers.txt', 'w') as fh: json.dump({'fords':outfords}, fh)
	#with open('rivers.txt', 'w') as fh: json.dump({'bridges':outbridges}, fh)

	#plt.imshow(bigvoronoi)
	#plt.imshow(bigelev, cmap=mapper.elevation, vmin=0, vmax=1)
	#plt.show()

	#bitmap = np.zeros((self.height, self.width, 3), dtype=np.uint8)

	#plt.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
	#plt.plot([p[1] for p in provinces], [p[0] for p in  provinces], lw=0, marker='.', color='r')
	#for p1 in ridges:
	#	for p2 in ridges[p1]:
	#		plt.plot([ridges[p1][p2][p3][1] for p3 in ridges[p1][p2]], [ridges[p1][p2][p3][0] for p3 in ridges[p1][p2]], color='r', marker='+')

	#plt.show()

	#fig = plt.figure()
	#ax = fig.gca()
	#ax.imshow(elevation, cmap=mapper.elevation, vmin=0, vmax=1)
	#from scipy.spatial import Voronoi, voronoi_plot_2d
	#vor = Voronoi([p[::-1] for p in provinces])
	#voronoi_plot_2d(vor, ax=ax)
	#ax.plot([p[1] for p in provinces], [p[0] for p in provinces], lw=0, marker='x', color='w')
	#ax.set_xlim([0,elevation.shape[1]])
	#ax.set_ylim([0,elevation.shape[0]])
	#fig.set_tight_layout(True)
	#fig.set_figwidth(12)
	#fig.set_figheight(9)
	#fig.savefig('qr.png')
	#plt.show()

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--width', type=int, default=2000)
	parser.add_argument('--height', type=int, default=1500)
	parser.add_argument('--resolution', type=float, default=2**-4, help='Resolution of parametric data relative to --width and --height')
	parser.add_argument('--provinces', type=int, default=150)
	parser.add_argument('--hwrap', action='store_true')
	parser.add_argument('--vwrap', action='store_true')
	parser.add_argument('--water', type=float, default=0.45)
	parser.add_argument('--mountains', type=float, default=0.2)
	parser.add_argument('--cave', type=float, default=0.05)
	parser.add_argument('--caveclustering', type=float, default=0.2)
	parser.add_argument('--ruggedness', type=float, default=0.3)
	parser.add_argument('--hillscraters', type=float, default=1.5)
	parser.add_argument('--seasize', type=float, default=3.5)
	parser.add_argument('--rivers', type=float, default=1.0)
	parser.add_argument('--extraisland', action='store_true')
	parser.add_argument('--seed', type=int)
	args = parser.parse_args()

	main(args)

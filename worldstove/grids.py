
import numpy as np

class Point(object):
	pos = None
	gridpos = None
	edges = None
	name = None
	color='tab:orange'
	def __init__(self, pos, gridpos, edges=None):
		self.pos = pos
		self.gridpos = gridpos
		self.edges = [] if edges is None else edges

	def __repr__(self): return 'Point({}, {}, edges={})'.format(*self.pos, len(self.edges))

	def __hash__(self): return hash(tuple(self.pos) + tuple(self.gridpos))

	def draw(self, ax):
		ax.scatter(self.pos[1], self.pos[0], color=self.color)
		if self.name: ax.annotate(self.name, (self.pos[1], self.pos[0]))
	
	def add_edge(self, edge):
		self.edges.append(edge)
	def remove_edge(self, edge):
		self.edges.remove(edge)

	def get_connections(self):
		connections = []
		for edge in self.edges:
			if edge.start is self: connections.append(edge.stop)
			else: connections.append(edge.start)
		return connections

	def __getitem__(self, index): return self.pos[index]
	def __setitem__(self, index, val): self.pos[index] = val

	def draw_connections(self, ax):
		for neighbor in self.get_connections():
			disp = np.array(neighbor.pos) - np.array(self.pos)
			ax.arrow(self[1], self[0], disp[1]/3, disp[0]/3, color='k')

def draw_pointlist(points, ax, color=None):
	X = [p[1] for p in points]
	Y = [p[0] for p in points]

class Edge(object):
	color = 'tab:cyan'
	def __init__(self, start, stop):
		self.start = start
		self.stop = stop

	def draw(self, ax):
		ax.plot((self.start[1], self.stop[1]), (self.start[0], self.stop[0]), color=self.color, lw=0.5)

class Grid(object):
	wrap = None
	shape = None
	spacing = None
	points = None
	edges = None
	color = 'tab:green'
	_grid = None
	def __init__(self, shape, spacing, wrap):
		self.wrap = wrap
		self.spacing = spacing
		self.shape = shape
		self.points = []
		self.edges = []
		self._grid = []
		self.initialize()

	def initialize(self):

		#self.shape = [self.shape[i] + (not self.wrap[i]) for i in range(2)]

		for r in range(self.shape[0]):
			self._grid.append([])
			for c in range(self.shape[1]):
				pos = (r+0.5)*self.spacing[0], (c+0.5)*self.spacing[1]
				self.points.append(Point(pos, (r,c)))
				self._grid[-1].append(self.points[-1])
				

		for r in range(self.shape[0]):
			for c in range(self.shape[1]):
				startpos = (r,c)
				start = self[startpos]
				for dr,dc in (1,0), (0,1):
					if self.wrap[0]: rstop = (r + dr) % self.shape[0]
					#elif r == self.shape[0] - 1: continue
					else: rstop = r + dr

					if self.wrap[1]: cstop = (c + dc) % self.shape[1]
					#elif c == self.shape[1] - 1: continue
					else: cstop = c + dc

					stoppos = rstop,cstop
			
					try: stop = self[stoppos]
					except IndexError: continue

					self.add_edge(start, stop)

	#def snap_point(self, point, strict=False): return np.int64(np.round(point / self.spacing))
	def snap_point(self, point, strict=False): return np.int64(point / self.spacing)

	def snap_points(self, pointlist, strict=False):
		outlist = []
		uniq = set()
		for pos in pointlist:
			gridpos = np.int64(np.round(pos / self.spacing))
			outlist.append(gridpos)

			if strict and tuple(gridpos) in uniq: raise ValueError('Collision at {}!'.format(tuple(gridpos)))
			uniq.add(tuple(gridpos))
		return outlist

	def __getitem__(self, index):
		if isinstance(index, int): return self._grid[index]
		else: return self._grid[index[0]][index[1]]
	def __setitem__(self, index, value):
		if isinstance(index, int): self._grid[index] = value
		else: self._grid[index[0]][index[1]] = value
		
	def add_edge(self, start, stop):
		#TODO: also allow explicit gridpos instead of getting it from Point objects
		edge = Edge(self[start.gridpos], self[stop.gridpos])
		self[start.gridpos].add_edge(edge)
		self[stop.gridpos].add_edge(edge)
		self.edges.append(edge)

	def get_edge(self, startpos, stoppos):
		#TODO: also allow explicit gridpos instead of getting it from Point objects but in reverse
		for edge in self[startpos].edges:
			if edge.start.gridpos == stoppos or edge.stop.gridpos == startpos: return edge
		raise IndexError('Could not find an edge bridging {} and {}'.format(startpos, stoppos))

	def draw(self, ax):
		for edge in self.edges: edge.draw(ax)

			
		#for r in range(self.shape[0]):
		#	ax.plot(
		#		[0, self.shape[1] * self.spacing[1]], 
		#		[r * self.spacing[0], r * self.spacing[0]], 
		#	color=self.color)

		#for c in range(self.shape[1]):
		#	ax.plot(
		#		[c * self.spacing[1], c * self.spacing[1]], 
		#		[0, self.shape[0] * self.spacing[0]], 
		#	color=self.color)

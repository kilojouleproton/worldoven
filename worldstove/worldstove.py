#!/usr/bin/env python

import coulomb
import grids
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from mapnukeparser import load_nations

class Generator(object):

	def __init__(self, shape, nations):

		if isinstance(nations, int): self.nations = [None, None, None, None]


class Viewer(coulomb.Viewer):
	def onkeydown(self, event):
		print(event.key)

def get_grid_spacing(shape, count, aspect):
	#find h, w for which
	#w/h == aspect
	#w == aspect*h
	#h == w/aspect
	#w*h*count ~= shape[0]*shape[1]
	#w*h ~= totalarea/count
	#h**2 * aspect ~= totalarea/count
	#h ~= totalarea/count/aspect
	#or
	#w**2/aspect ~= totalarea/count
	#w**2 ~= totalarea/count*aspect
	totalarea = shape[0] * shape[1]
	gridspacing = np.int64(np.round(((totalarea/aspect/count)**.5, (totalarea*aspect/count)**.5)))
	newshape = np.int64(np.ceil((shape[0]/gridspacing[0], shape[1]/gridspacing[1])) * gridspacing)
	return gridspacing, newshape, newshape//gridspacing

	#(shape[0]/np.round(gridspacing[0]), shape[1]/np.round(gridspacing[1]))

def generate_diagonals(grid, capringsize=5):
	for r in range(grid.shape[0]):
		for c in range(grid.shape[1]):

			rstop = r + 1
			cstop = c + 1
			if grid.wrap[0]: rstop = rstop % grid.shape[0]
			if grid.wrap[1]: cstop = cstop % grid.shape[1]

			if rstop >= grid.shape[0]: continue
			if cstop >= grid.shape[1]: continue

			roll = np.random.randint(2)
			if roll: 
				startpos = r,c
				stoppos = rstop,cstop
			else: 
				startpos = r,cstop
				stoppos = rstop,c
			start = grid[startpos]
			stop = grid[stoppos]
			grid.add_edge(start, stop)

	for r in range(grid.shape[0]):
		for c in range(grid.shape[1]):
			if grid[r,c].name is None: continue
			#print('before', r, c, grid[r,c].name, grid[r,c])

			maxiter = 10
			n = 0
			while len(grid[r,c].edges) < capringsize:
				n += 1
				if n > maxiter: break
				choices = []
				startpos = r,c
				for dr,dc in ((-1,-1), (-1,1), (1,1), (1,-1)):
					rstop = r+dr
					cstop = c+dc
					if grid.wrap[0]: rstop = rstop % grid.shape[0]
					if grid.wrap[1]: cstop = cstop % grid.shape[1]

					if not (0 <= rstop < grid.shape[0]): continue
					if not (0 <= cstop < grid.shape[1]): continue

					if grid[rstop,cstop].name is None: choices.append((rstop,cstop))
				if not choices:
					#print('Could not add an edge for {}'.format((r,c)))
					break
				stoppos = choices[np.random.randint(len(choices))]

				try: edge = grid.get_edge((startpos[0],stoppos[1]), (stoppos[0],startpos[1]))
				except IndexError: 
					#print('Failed to flip a suitable edge at {}-{}'.format((r,c), stoppos))
					break
				#print('Flipped an edge to expand capring at {}'.format((r,c)))
				oldstart = edge.start
				oldstop = edge.stop

				newstart = grid[oldstart.gridpos[0], oldstop.gridpos[1]]
				newstop = grid[oldstop.gridpos[0], oldstart.gridpos[1]]

				edge.start.remove_edge(edge)
				edge.stop.remove_edge(edge)
				newstart.add_edge(edge)
				newstop.add_edge(edge)

				edge.start = newstart
				edge.stop = newstop

			n = 0
			while len(grid[r,c].edges) > capringsize:
				n += 1
				if n > maxiter: break
				if len(grid[r,c].edges) == 4: break
				choices = []
				for edge in grid[r,c].edges:
					if edge.start.gridpos[0] == edge.stop.gridpos[0]: continue
					if edge.start.gridpos[1] == edge.stop.gridpos[1]: continue
					if (edge.start.name is None) or (edge.stop.name is None): choices.append(edge)

				if not choices: 
					#print('Could not prune {} ({} edges)'.format((r,c), len(grid[r,c].edges)))
					break
				edge = np.random.choice(choices)
				#print('Pruned an edge for {}, {} ({}-{})'.format(grid[r,c], (r,c), edge.start.gridpos, edge.stop.gridpos))

				#edge = grid[r,c].edges[roll]
				oldstart = edge.start
				oldstop = edge.stop

				newstart = grid[oldstart.gridpos[0], oldstop.gridpos[1]]
				newstop = grid[oldstop.gridpos[0], oldstart.gridpos[1]]

				edge.start.remove_edge(edge)
				edge.stop.remove_edge(edge)
				edge.start = newstart
				edge.stop = newstop
				newstart.add_edge(edge)
				newstop.add_edge(edge)
					
			#print('after', r, c, grid[r,c].name, grid[r,c])

def perturb_grid(grid, maxdisp=0.2):
	for p in grid.points:
		p.pos = [p.pos[i] + maxdisp * grid.spacing[i] * np.random.uniform(-1, 1) for i in range(2)]

def compute_borders(grid):
	colors = ['tab:{}'.format(col) for col in ('blue', 'orange', 'green', 'red', 'purple', 'brown', 'pink', 'gray', 'olive', 'cyan')]
	polygons = []
	for p in grid.points:
		vertices = []

		connected = p.get_connections()
		connected.sort(key=lambda v: np.arctan2(v.pos[0] - p.pos[0], v.pos[1] - p.pos[1]))

		for i, p1 in enumerate(connected):
			p2 = connected[(i+1) % len(connected)]
			vertices.append(np.mean([p.pos, p1.pos, p2.pos], axis=0))

		if p.name is None: color = 'tab:green'
		elif 'start' in p.name: color = 'tab:red'
		elif 'throne' in p.name: color = 'tab:purple'
		else: color = 'tab:green'
		#polygons.append(mpatches.Polygon([v[::-1] for v in vertices], facecolor=np.random.choice(colors)))
		polygons.append(mpatches.Polygon([v[::-1] for v in vertices], facecolor=color, lw=1, edgecolor='white'))

	return polygons
			

def test_stove(nations=5, thrones=5, provinces=None, aspect=1/0.58, shape=(1500,3000), wrap=(False, False), delta=5, capringsize=5):
	if provinces is None: provinces = nations * 15
	gridspacing, shape, gridshape = get_grid_spacing(shape, provinces, aspect)

	grid = grids.Grid(gridshape, gridspacing, wrap)

	#generate thrones and capitals
	constant = (shape[0] * shape[1])**2 * 0.004
	testmap = coulomb.System(shape=shape, wrap=wrap, scale=constant, delta=delta)

	for i in range(thrones):
		testmap.add(coulomb.Particle(pos=np.random.uniform(shape), name='throne{}'.format(i)))
	testmap.relax()
	for ent in testmap.entities: ent.fixed = True
	for i in range(nations):
		testmap.add(coulomb.Particle(pos=np.random.uniform(shape), name='start{}'.format(i), color='tab:red'))
	testmap.relax()

	for p in testmap.entities:
		gridpos = grid.snap_point(p.pos, strict=True)
		grid[gridpos].name = p.name
		if 'throne' in p.name: grid[gridpos].color = 'tab:blue'
		else: grid[gridpos].color = 'tab:red'

	generate_diagonals(grid, capringsize=capringsize)

	perturb_grid(grid, maxdisp=0.2)

	borders = compute_borders(grid)


	#viewer = coulomb.Viewer(testmap)
	#viewer.fig.set_tight_layout(True)
	#viewer.ax.set_xlim([0, shape[1]])
	#viewer.ax.set_ylim([0, shape[0]])
	#viewer.redraw(force=False)
	fig, ax = plt.subplots()
	fig.set_tight_layout(True)
	ax.set_xlim([0, shape[1]])
	ax.set_ylim([0, shape[0]])

	grid.draw(ax)
	for p in grid.points: p.draw(ax)
	[ax.add_patch(p) for p in borders]
	
	plt.show()
	with open('Nations/Default.xml') as fh: load_nations(fh)

if __name__ == '__main__':
	test_stove()

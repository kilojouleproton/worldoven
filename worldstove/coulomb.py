#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

class Particle(object):
	pos = None
	vel = None
	acc = None

	mass = 1
	charge = 1
	thronecharge = -1
	radius = 200
	depth = 4e-8
	falloff = 6
	
	fixed = False

	color = 'tab:blue'

	name = None

	def __init__(self, pos, vel=None, acc=None, fixed=False, mass=1, charge=1, color='tab:blue', name=None, thronecharge=-1):
		self.pos = np.array(pos)
		self.vel = np.zeros(2) if vel is None else vel
		self.acc = np.zeros(2) if acc is None else acc
		self.mass = mass
		self.charge = charge
		self.thronecharge = thronecharge

		self.fixed = fixed

		self.name = name
		self.color = color

		self.plotobjects = []

	def ljinteract(self, other, displacement, **kwargs):
		distance = np.linalg.norm(displacement)

		if distance > 4 * self.radius: return

		direction = displacement / distance
		sigma = self.radius + other.radius
		force = 4 * self.depth * (-self.falloff*2 * (sigma/distance)**(self.falloff*2) - -self.falloff*(sigma/distance)**self.falloff) / distance
		force = np.clip(force, -self.radius, self.radius)
		force *= direction 


		self.acc -= force / self.mass

	def interact(self, other, displacement, **kwargs):
		distance = np.linalg.norm(displacement)
		direction = displacement / distance
		depth = kwargs.get('depth')
		thronedepth = kwargs.get('thronedepth')
		#nonspecific repulsion
		force = 0
		force += depth * self.charge * other.charge / distance**2 * direction
		#throne force
		#force += thronedepth * self.thronecharge * other.thronecharge / distance**2 * direction


		self.acc += force / self.mass
		
	def draw(self, ax, clear=False, force=False):
		if clear and not self.fixed:
			for thing in self.plotobjects: thing.remove()
			self.plotobjects = []

		if force or not (self.fixed and self.plotobjects):

			self.plotobjects.append(ax.scatter(self.pos[1], self.pos[0], color=self.color))
			self.plotobjects.append(ax.annotate(
				self.name,
				xy=(self.pos[1], self.pos[0])
			))
			#self.plotobjects.append(ax.annotate(
			#	'{:0.1e},{:0.1e}'.format(*self.acc), 
			#	xy=(self.pos[1], self.pos[0]), 
			#	#xytext=(self.pos[1]+self.acc[1], self.pos[0]+self.acc[0]), 
			#	#arrowprops={'arrowstyle':'->'}
			#))
			self.plotobjects.append(ax.arrow(
				self.pos[1], self.pos[0],
				self.acc[1], self.acc[0],
				length_includes_head=True,
				width=0.1,
			))

def wrapped_displacement(here, there, shape, wrap):
	disp = np.zeros(2)

	for axis in range(2):
		if wrap[axis]: 
			raw = here.pos[axis] - there.pos[axis]
			if here.pos[axis] < there.pos[axis]:
				wra = -shape[axis] - raw
			else:
				wra = shape[axis] - raw
			wra *= -1

			if abs(raw) < abs(wra): disp[axis] = raw
			else: disp[axis] = wra
		else: disp[axis] = here.pos[axis] - there.pos[axis]

	return disp

class System(object):
	entities = None
	shape = None
	wrap = (True, True)
	drag = 0.00
	sqdrag = 0.1
	expdrag = 0.95
	scale = 8000
	#thronescale = 8000
	thronescale = 2000
	delta = 1
	wallcharge = 0.5

	iteration = 0

	def __init__(self, shape, wrap=(False, True), scale=8000, thronescale=8000, delta=1): 
		self.shape = shape
		self.wrap = wrap
		self.entities = []
		self.scale = scale
		self.thronescale = thronescale
		self.delta = delta

	def add(self, entity):
		self.entities.append(entity)

	def step(self, delta=None):
		self.iteration += 1

		delta = self.delta if delta is None else delta
		oldacc=  []

		for i1, p1 in enumerate(self.entities):

			oldacc.append(p1.acc * 1)
			if p1.fixed: continue

			p1.acc = np.zeros(2)

			if not self.wrap[0]:
				if p1.pos[0] < self.shape[0]/2: 
					wall = 1
					dist = max(1, p1.pos[0])
				else: 
					wall = -1
					dist = max(1, self.shape[0] - p1.pos[0])
				p1.acc += self.scale * p1.charge * self.wallcharge / p1.mass * np.array([wall,0]) / (dist)**2

			if not self.wrap[1]:
				if p1.pos[1] < self.shape[1]/2: 
					wall = 1
					dist = max(1, p1.pos[1])
				else: 
					wall = -1
					dist = max(1, self.shape[1] - p1.pos[1])
				p1.acc += self.scale * p1.charge * self.wallcharge / p1.mass * np.array([0,wall]) / (dist)**2


			for i2, p2 in enumerate(self.entities):
				if i1 == i2: continue

				dy, dx = r = wrapped_displacement(p1, p2, self.shape, self.wrap)

				#p1.acc += self.scale * p1.charge * p2.charge / p1.mass * np.array([dy,dx]) / dist**3
				p1.interact(p2, r, depth=self.scale, thronedepth=self.thronescale)

			#if self.sqdrag:
			#	p1.acc += -self.sqdrag * p1.vel**2 * np.sign(p1.vel)
			#elif self.drag:
			#	p1.acc += -self.drag * p1.vel

		for i, p in enumerate(self.entities):
			if p.fixed: continue

			p.vel[0] = np.clip(p.vel[0], -self.shape[0]/4/delta, self.shape[0]/4/delta)
			p.vel[1] = np.clip(p.vel[1], -self.shape[1]/4/delta, self.shape[1]/4/delta)
			p.acc[0] = np.clip(p.acc[0], -2*self.shape[0]/delta**3/4, 2*self.shape[0]/delta**3/4)
			p.acc[1] = np.clip(p.acc[1], -2*self.shape[1]/delta**3/4, 2*self.shape[1]/delta**3/4)
			p.pos += p.vel*delta + 0.5*p.acc*delta**2
			p.vel += (oldacc[i] + p.acc) / 2

			p.vel *= self.expdrag

			if self.wrap[0]: p.pos[0] = p.pos[0] % self.shape[0]
			else: p.pos[0] = np.clip(p.pos[0], 4, self.shape[0]-4)
			if self.wrap[1]: p.pos[1] = p.pos[1] % self.shape[1]
			else: p.pos[1] = np.clip(p.pos[1], 4, self.shape[1]-4)

	def relax(self, cycles=100, delta=None):
		for i in range(cycles): self.step(delta)

class Viewer(object):
	fig = None
	ax = None
	tainted = False

	def __init__(self, system, ax=None):
		self.system = system
		self.fig, self.ax = plt.subplots()

		self.fig.canvas.mpl_connect('key_press_event', self.onkeydown)

		self.redraw()

	def onkeydown(self, event):
		if event.key == 'n': 
			self.system.step()
			self.tainted = True
		elif event.key == 'r': 
			if self.tainted: self.redraw()
		elif event.key == 'tab':
			for i in range(10): self.system.step()
			self.redraw(clear=True)
			#self.ax.set_title('Mean speed: {:0.1e}'.format(np.mean([np.linalg.norm(p.vel) for p in self.system.entities])))
			self.ax.set_title('Mean KE: {:0.1e} ({}x{})'.format(np.mean([0.5*p.mass*np.sum(np.square(p.vel)) for p in self.system.entities]), self.system.iteration, self.system.delta))
		elif event.key == 'c':
			self.redraw(clear=True)

		elif event.key == 'v':
			self.redraw(clear=True, force=True)

		elif event.key == 't':
			totalvel = np.sum([p.vel for p in self.system.entities], axis=0)
			print('Net velocity: {:0.2e}, {:0.2e}'.format(*totalvel))	

		elif event.key == ',':
			self.system.expdrag *= 0.9
			print(self.system.expdrag)
		elif event.key == '.':
			self.system.expdrag /= 0.9
			print(self.system.expdrag)
		else: print(event.key)

	def redraw(self, clear=False, force=False):
		for entity in self.system.entities:
			entity.draw(self.ax, clear=clear, force=force)
		self.fig.canvas.draw()
		self.tainted = False
		

if __name__ == '__main__':
	seed = np.random.randint((1<<32)-1)
	np.random.seed(seed)
	shape = (2000000, 4000000)
	shape = (200, 400)
	shape = (1500, 2000)
	constant = 8000
	constant = (shape[0] * shape[1]) ** 2 * 0.004
	throneconstant = (shape[0] * shape[1]) ** 2 * 0.000
	testmap = System(shape=shape, wrap=(False, True), scale=constant, thronescale=throneconstant)
	testmap.delta = 5

	nations = 5
	thrones = 5

	for i in range(thrones):
		testmap.add(Particle(pos=(np.random.uniform(shape[0]), np.random.uniform(shape[1])), charge=1, color='tab:orange', name='throne{}'.format(i), thronecharge=1))
	testmap.relax()

	#viewer = Viewer(testmap)
	#viewer.ax.set_xlim([0, shape[1]])
	#viewer.ax.set_ylim([0, shape[0]])
	#
	#plt.show()

	for ent in testmap.entities: ent.fixed = True

	for i in range(nations):
		testmap.add(Particle(pos=(np.random.uniform(shape[0]), np.random.uniform(shape[1])), name='start{}'.format(i)))

	testmap.relax()

	viewer2 = Viewer(testmap)
	viewer2.fig.set_tight_layout(True)
	viewer2.ax.set_xlim([0, shape[1]])
	viewer2.ax.set_ylim([0, shape[0]])
	viewer2.redraw(force=True)

	viewer2.ax.set_title('Seed: {} ({}iters x {})'.format(seed, testmap.iteration, testmap.delta))

	viewer2.fig.set_figwidth(8)
	viewer2.fig.set_figheight(6)
	viewer2.fig.savefig('test.png')
	#plt.show()

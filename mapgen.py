#!/usr/bin/env python
import mapper
import numpy as np

class Dom5MapGenerator(mapper.ComplexMap):
	def __init__(self, width=None, height=None, provinces=None, hwrap=None, vwrap=None, water=0.45, cave=0.05, caveclustering=0.2, ruggedness=0.3, hillscraters=150, seasize=3.5, rivers=1.0, extraisland=0.0):
		super().__init__(width=width, height=height, hwrap=hwrap, vwrap=vwrap)


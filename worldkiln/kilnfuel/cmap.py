import matplotlib.colors

_elevation_data = (
    (0.00, 0.00, 0.75), #deep sea
    (0.00, 0.50, 0.75), #shallow sea
    (0.50, 0.75, 0.25), #lowland
    (0.50, 0.75, 0.25), #lowland
    (0.00, 0.50, 0.00), #midland
    (0.00, 0.50, 0.00), #midland
    (0.50, 0.25, 0.00), #highland
    (0.75, 0.75, 0.75), #highland
)

elevation = matplotlib.colors.ListedColormap(_elevation_data, name='elevation')

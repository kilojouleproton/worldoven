import numpy as np
np.random.seed(1)
from scipy import ndimage

def white_noise(shape, low=0, high=1):
	return np.random.random(shape) * (high - low) + low

def low_pass_gaussian(arr, radius=None, maxfreq=None, normalize=True):
	if radius is None and maxfreq is None: raise TypeError('Please specify either filter radius or maximum frequency')
	elif not (radius is None or maxfreq is None): raise TypeError('Please specify either filter radius or maximum frequency')

	elif radius:
		fft = np.fft.fft2(arr)
		newarr = np.real(np.fft.ifft2(ndimage.fourier_gaussian(fft, sigma=radius)))
	else:
		fft = np.fft.fft2(arr)
		try: newarr = np.real(np.fft.ifft2(ndimage.fourier_gaussian(fft, sigma=(arr.shape[0]/maxfreq[0], arr.shape[1]/maxfreq[1]))))
		except TypeError: newarr = np.real(np.fft.ifft2(ndimage.fourier_gaussian(fft, sigma=(arr.shape[0]/maxfreq, arr.shape[1]/maxfreq))))
	if normalize: return _normalize(newarr)
	else: return newarr

def high_pass_gaussian(arr, radius=None, minfreq=None, normalize=True):
	newarr = arr - low_pass_gaussian(arr, radius=radius, maxfreq=minfreq)
	if normalize: return _normalize(newarr)
	else: return newarr

def band_pass_gaussian(arr, minrad=None, maxrad=None, normalize=True):
	newarr = arr - low_pass_gaussian(arr, radius=maxrad) - high_pass_gaussian(arr, radius=minrad)
	if normalize: return _normalize(newarr)
	else: return newarr

def _normalize(arr, newmin=0, newmax=1):
	mindiff = np.min(arr) - newmin
	arrrange = np.max(arr) - np.min(arr)
	newrange = newmax - newmin
	return (arr - mindiff) * newrange / arrrange

def brown_noise(shape, radius=None, maxfreq=None, normalize=True): return low_pass_gaussian(white_noise(shape), radius=radius, maxfreq=maxfreq, normalize=normalize)

def green_noise(shape, minrad=None, maxrad=None, minfreq=None, maxfreq=None, normalize=True):
	if not (minfreq is None and maxfreq is None): raise NotImplementedError
	return band_pass_gaussian(white_noise(shape), minrad=minrad, maxrad=maxrad, normalize=normalize)

def blue_noise(shape, radius=32, normalize=True): return high_pass_gaussian(white_noise(shape), radius=radius, normalize=normalize)

def amplify(arr, radius=None, maxfreq=None, factor=4):
	angle = np.random.random() * np.pi * 2
	if radius is None and maxfreq is None: raise TypeError('Please specify either filter radius or maximum frequency')
	elif not (radius is None or maxfreq is None): raise TypeError('Please specify either filter radius or maximum frequency')

	elif radius:
		r = int(np.round(arr.shape[0] / radius * np.sin(angle)))
		c = int(np.round(arr.shape[1] / radius * np.cos(angle)))
	else:
		r = int(np.round(maxfreq * np.sin(angle)))
		c = int(np.round(maxfreq * np.cos(angle)))
		
	fft = np.fft.fft2(arr)
	fft[r,c] *= factor
	fft[-r,-c] *= factor
	return np.real(np.fft.ifft2(fft))
	return arr

def crop(arr, factor):
	return arr[
		int(arr.shape[0]*factor/2):-int(arr.shape[0]*factor/2), 
		int(arr.shape[1]*factor/2):-int(arr.shape[1]*factor/2), 
	]

def _throw_dart(center, minrad):
	r = (np.random.random() + 1) * minrad
	theta = np.random.random() * 2 * np.pi
	return np.array(center + [r*np.cos(theta), r*np.sin(theta)])

def _in_rect(point, shape):
	if (0 <= point[0] < shape[0]) and (0 <= point[1] < shape[1]): return True
	else: return False

def _no_neighbor(point, shape, cell, grid, mindist, outlist, wrap=None):
	wrap = (0,0) if wrap is None else wrap
	closest = shape[1]**2

	minsqdist = mindist**2

	for dr in range(-2, 2+1):
		newr = cell[0] + dr
		if wrap[0]: newr = newr % len(grid)
		if not (0 <= newr < len(grid)): continue

		for dc in range(-2, 2+1):
			newc = cell[1] + dc
			if wrap[1]: newc = newc % len(grid[0])
			if not (0 <= newc < len(grid[0])): continue

			for oldindex in grid[newr][newc]:
				oldpoint = outlist[oldindex]
				vec = point - oldpoint
				if wrap[0]: vec[0] = min(abs(vec[0]), abs(vec[0] + shape[0]), abs(vec[0] - shape[0]))
				if wrap[1]: vec[1] = min(abs(vec[1]), abs(vec[1] + shape[1]), abs(vec[1] - shape[1]))
				sqdist = np.sum(np.square(vec))
				if sqdist < closest: closest = sqdist
				#print('comparing in cell {} to {}'.format(cell, oldindex), oldpoint, point, sqdist, minsqdist)
				if sqdist < minsqdist: 
					#print('rejected')
					return False
	#print('closest', closest**.5)
	return True
	

def poisson_disk(shape, density=None, targetnum=None, base_mindist=None, attempts=30, initial_points=None, wrap=None):
	if targetnum is None and base_mindist is None:
		raise TypeError('Please specify either targetnum or base_mindist')
	elif not (targetnum is None or base_mindist is None):
		raise TypeError('Please specify either targetnum or base_mindist')
	elif targetnum is None:
		if density is None: density = np.ones(shape)
		cellsize = base_mindist / np.min(density) / 2 ** 0.5
	elif base_mindist is None:
		if density is None: density = np.ones(shape)
		#old formula: 3 * (area/1000000)**.537 * (1/360000*len(provinces))**-0.537
		#base_mindist = 3 * (shape[0]*shape[1] * 0.36 / targetnum) ** .5


		#assume mean distance of 3r (1.5r * 2)
		#assume packing coefficient between 78% (square packing) and 90% (hex packing)
		#k = 0.85

		k = 0.90
		base_mindist = ((shape[0] * shape[1] * k) / np.pi / targetnum * 2) ** 0.5 
		#A = n * pi*r**2 / k
		#r**2 = A/n/pi * k
		#r = sqrt(A*k/n/pi)
		#cellsize = base_mindist / 2**.5
		#cellsize = shape[1]
		cellsize = base_mindist / np.min(density) / 2 ** 0.5

	#modality 1: given base_mindist, pick a bunch of points
	#modality 1: given targetnum, estimate base_mindist

	cellsize = np.min((min(shape), cellsize))
	gridshape = (max(1, int(np.ceil(shape[0]/cellsize))), max(1, int(np.ceil(shape[1]/cellsize))))
	grid = []
	for r in range(gridshape[0]):
		grid.append([])
		for c in range(gridshape[1]):
			grid[-1].append([])
			

	wrap = (0, 1) if wrap is None else wrap

	proclist = []
	outlist = []

	if initial_points:
		proclist = initial_points[:]
		outlist = initial_points[:]
		for i, p in enumerate(initial_points):
			r, c = int(p[0]/cellsize), int(p[1]/cellsize)
			grid[r][c].append(len(outlist)-1)

	else:
		point = np.random.random(2) * [shape[0]-1, shape[1]-1]
		proclist.append(point)
		outlist.append(point)
		r, c = int(point[0]/cellsize), int(point[1]/cellsize)
		grid[r][c].append(len(outlist)-1)

	while proclist:
		#print(len(proclist), len(outlist))
		oldpoint = proclist.pop(np.random.randint(len(proclist)))
		oldcell = int(oldpoint[0]/cellsize), int(oldpoint[1]/cellsize)
		for i in range(attempts):
			newpoint = _throw_dart(oldpoint, base_mindist/density[int(oldpoint[0]), int(oldpoint[1])])
			#newpoint = _throw_dart(oldpoint, base_mindist/density[int(newpoint[0]), int(newpoint[1])])
			if wrap[0]: newpoint[0] = newpoint[0] % shape[0]
			if wrap[1]: newpoint[1] = newpoint[1] % shape[1]

			if not _in_rect(newpoint, shape): 
				#print('rejected: not in rect')
				continue

			newcell = int(newpoint[0]/cellsize), int(newpoint[1]/cellsize)

			if not _no_neighbor(newpoint, shape, newcell, grid, base_mindist/density[int(oldpoint[0]), int(oldpoint[1])], outlist, wrap=wrap): 
			#if not _no_neighbor(newpoint, shape, newcell, grid, base_mindist/density[int(newpoint[0]), int(newpoint[1])], outlist, wrap=wrap): 
				#print('rejected: too close')
				continue

			outlist.append(newpoint)
			proclist.append(newpoint)
			grid[newcell[0]][newcell[1]].append(len(outlist) - 1)

	return outlist

#!/usr/bin/env python
import argparse
import kilnfuel
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from scipy import ndimage
from scipy.spatial import Voronoi, voronoi_plot_2d
from PIL import Image
import os
import numpy as np
import json

def generate_elevation(shape, seapart=0.3, mountpart=0.2, hills=1.5, ruggedness=0.3, wrap=(0,1)):
	#elevation = kilnfuel.noise.brown_noise(shape, maxfreq=8, normalize=True)
	workshape = []
	workshape.append(shape[0] if wrap[0] else shape[0]*2)
	workshape.append(shape[1] if wrap[1] else shape[1]*2)
	#TODO: work exclusively with frequencies instead of radii
	elevation = kilnfuel.noise.brown_noise(workshape, radius=200)

	#apply hills
	elevation += kilnfuel.noise.brown_noise(workshape, radius=100) * hills / 2

	#apply ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=64) / 1 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=32) / 2 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=16) / 4 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=8) / 8 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=4) / 16 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=2) / 32 * ruggedness
	elevation += kilnfuel.noise.brown_noise(workshape, radius=1) / 64 * ruggedness

	elevation -= np.min(elevation)
	elevation /= np.max(elevation)

	#finally, normalize to specified sea/mountain percentages
	freqs, edges = np.histogram(elevation, bins=50)
	total = np.sum(freqs)
	runtot = 0
	searight = None
	mountleft = None

	lastside = 0
	for f, leftside in zip(freqs, edges):
		curfrac = (runtot + f)/total
		if searight is None:
			if (curfrac-f/total) > seapart:
				searight = lastside
		elif mountleft is None:
			if (curfrac-f/total) > (1 - mountpart):
				mountleft = leftside
		else: break
		runtot += f
		lastside = leftside
	lowelevs = elevation < searight
	highelevs = elevation >= mountleft
	elevation[lowelevs] -= np.min(elevation[lowelevs])
	elevation[lowelevs] *= 0.25 / np.max(elevation[lowelevs])

	elevation[highelevs] -= mountleft
	elevation[highelevs] *= 0.25 / np.max(elevation[highelevs])
	elevation[highelevs] += 0.75
	
	midelevs = (searight <= elevation) & (elevation < mountleft)
	elevation[midelevs] -= np.min(elevation[midelevs])
	elevation[midelevs] /= np.max(elevation[midelevs])
	elevation[midelevs] *= 0.5
	elevation[midelevs] += 0.25
	#elevation[midelevs] = elevation[midelevs] * 0.5 + 0.25

	elevation = elevation[:shape[0], :shape[1]]
	return elevation

def generate_province_centers(shape, wrap, elevation, seasize=3.5, mapprov=50):
	density = np.ones(shape)
	density[elevation < 0.375] /= seasize ** 0.5
	density[elevation < 0.25] /= seasize
	density[elevation < 0.125] /= seasize ** 1.5
	points = kilnfuel.noise.poisson_disk(shape, density=density, targetnum=mapprov, wrap=wrap)
	return points

	#plt.imshow(elevation, cmap=kilnfuel.cmap.elevation, vmin=0, vmax=1)
	#plt.plot([p[1] for p in points], [p[0] for p in points], lw=0, marker='.', color='r')
	#plt.show()

def get_symmetry_operators(wrap):
	if wrap[0]: 
		sy = [1, 1, 1]
		Ty = [0, -1, 1]
	else: 
		sy = [1, -1, -1]
		Ty = [0, 0, 2]

	if wrap[1]:
		sx = [1, 1, 1]
		Tx = [0, -1, 1]
	else:
		sx = [1, -1, -1]
		Tx = [0, 0, 2]

	return [sy,sx], [Ty,Tx]

def expand_pointlist(shape, wrap, points):
	newpoints = []
	[sy, sx], [Ty, Tx] = get_symmetry_operators(wrap)

	for x in range(3):
		for y in range(3):
			newpoints.extend([p*[sy[y],sx[x]] + np.array(shape)*[Ty[y],Tx[x]] for p in points])
	return newpoints

def expand_array(shape, wrap, arr):
	newarr = np.zeros(np.array(arr.shape)*3)
	[sy, sx], [Ty, Tx] = get_symmetry_operators(wrap)

	for x in range(3):
		for y in range(3):
			topleft = shape[0]*y, shape[1]*x
			bottomright = shape[0]*(y+1), shape[1]*(x+1)
			newarr[topleft[0]:bottomright[0],topleft[1]:bottomright[1]] = arr[::sy[y],::sx[x]]
	return newarr

class Provgon(mpatches.Polygon): provid = None

def generate_province_areas(shape, wrap, elevation, points):
	#specify as orig, topleft, bottomright
	tilepoints = expand_pointlist(shape, wrap, points)
	bigelev = expand_array(shape, wrap, elevation)

	voronoi = Voronoi(np.roll(tilepoints, 1, axis=1))

	provmap = -np.ones(elevation.shape)
	for i, r in enumerate(voronoi.point_region):
		#print('{}/{} provinces painted'.format(i, len(voronoi.point_region)))
		provgon = Provgon([voronoi.vertices[v] for v in voronoi.regions[r] if v != -1])
		provgon.provid = i % len(points)

		remainingpoints = np.roll(np.vstack(np.nonzero(provmap == -1)).T, 1, axis=1)
		paintme = remainingpoints[provgon.contains_points(remainingpoints)]
		provmap[paintme[:,1],paintme[:,0]] = provgon.provid

	return provmap

def get_neighbors(provmap):
	vroll = np.roll(provmap, 1, axis=0)
	hroll = np.roll(provmap, 1, axis=1)

	neighbors = {}
	for here, there in zip(np.ravel(provmap[provmap != vroll]), np.ravel(vroll[vroll != provmap])):
		try: neighbors[here].add(there)
		except KeyError: neighbors[here] = set([there])
	for here, there in zip(np.ravel(provmap[provmap != hroll]), np.ravel(hroll[hroll != provmap])):
		try: neighbors[here].add(there)
		except KeyError: neighbors[here] = set([there])

	nlist = []
	for i in range(max(neighbors)):
		try: nlist.append(neighbors[i])
		except KeyError: nlist.append(set())

	return nlist

def generate_rivers(shape, wrap, elevation, points, riverpart=1.0):
	#specify as orig, topleft, bottomright
	tilepoints = expand_pointlist(shape, wrap, points)
	bigelev = expand_array(shape, wrap, elevation)

	voronoi = Voronoi(np.roll(tilepoints, 1, axis=1))

	vertelev = []
	for y, x in voronoi.vertices: 
		if -shape[0]/2 <= y < 3/2*shape[0] and -shape[1]/2 <= x < 3/2*shape[1]:
			vertelev.append(bigelev[int(x),int(y)])
		else:
			vertelev.append(-1)

	diffmap = {}
	for v, p in enumerate(voronoi.vertices): diffmap[v] = {}

	fig, ax = plt.subplots()
	ax.imshow(elevation, cmap=kilnfuel.cmap.elevation, vmin=0, vmax=1)
	#ax.imshow(elevation, cmap='terrain', vmin=0, vmax=1)
	voronoi_plot_2d(voronoi, ax=ax)

	for i, v in enumerate(voronoi.vertices):
		ax.annotate('{:0.2f}'.format(vertelev[i]), v[::1])

	for ridgeid, (v1, v2) in enumerate(voronoi.ridge_vertices):
		if v1 == -1 or v2 == -1: continue
		elev1 = vertelev[v1]
		elev2 = vertelev[v2]
		diffmap[v1][v2] = elev2 - elev1
		diffmap[v2][v1] = elev1 - elev2
	
		if elev1 > elev2:
			x = voronoi.vertices[v1][1]
			y = voronoi.vertices[v1][0]
			dx = voronoi.vertices[v2][1] - voronoi.vertices[v1][1]
			dy = voronoi.vertices[v2][0] - voronoi.vertices[v1][0]
		elif elev2 > elev1:
			x = voronoi.vertices[v2][1]
			y = voronoi.vertices[v2][0]
			dx = voronoi.vertices[v1][1] - voronoi.vertices[v2][1]
			dy = voronoi.vertices[v1][0] - voronoi.vertices[v2][0]
		else:
			x = 0
			y = 0
			dx = 1
			dy = 1
		#ax.arrow(x,y,dx,dy, head_width=20, length_includes_head=True)
		ax.arrow(y,x,dy,dx, head_width=20, length_includes_head=True)

	downslopes = {}
	for ridgeid in diffmap:
		downslopes[ridgeid] = [k for k in diffmap[ridgeid] if (diffmap[ridgeid][k] < 0) and (diffmap[ridgeid][k] == min(diffmap[ridgeid].values()))][:1]
		if downslopes[ridgeid]: downslopes[ridgeid] = downslopes[ridgeid][0]
		else: downslopes[ridgeid] = None

	#ax.arrow(100,100,1000,1000, head_width=20, length_includes_head=True)
	riverquota = int(np.ceil(riverpart * 10))
	riverqueue = []
	for i, elev in enumerate(vertelev):
		if elev < 0.5: continue
		elif 0 <= voronoi.vertices[i][0] <= (shape[0] - 1) and 0 <= voronoi.vertices[i][1] <= (shape[1]-1):
			riverqueue.append(i)
	np.random.shuffle(riverqueue)
	while riverquota and riverqueue:
		v = riverqueue.pop()
		point = voronoi.vertices[v]
		print(point)
		river = [v]
		while 1:
			nextvert = downslopes[river[-1]]
			if nextvert is None: break
			river.append(nextvert)
		print(river)

		ax.scatter(point[0], point[1], marker='.', color='c', s=5000)
		riverquota -= 1
		

	ax.set_xlim([0, shape[1]])
	ax.set_ylim([0, shape[0]])
	plt.show()

def main(args):
	shape = args.mapsize[::-1]
	wrap = [args.vwrap, not args.nohwrap]

	if not os.path.isdir(args.o): os.mkdir(args.o)

	fn = '{}/elevation.png'.format(args.o)
	if os.path.isfile(fn):
		if not args.quiet: print('Loading elevation...')
		elevation = np.float64(Image.open('{}/elevation.png'.format(args.o)))/(2**16-1)
	else:
		if not args.quiet: print('Generating elevation...')
		elevation = generate_elevation(shape, wrap=wrap, seapart=args.seapart, mountpart=args.mountpart, hills=args.hills, ruggedness=args.ruggedness)
		#plt.imshow(np.uint16(elevation * (2**16-1)))
		im = Image.fromarray(np.uint16(elevation * (2**16-1)))
		im.save(fn)

	fn = '{}/centers.json'.format(args.o)
	if os.path.isfile(fn):
		if not args.quiet: print('Loading province centers...')
		with open(fn) as fh: province_centers = np.float64(json.load(fh))
	else:
		if not args.quiet: print('Generating province centers...')
		province_centers = generate_province_centers(shape, wrap, elevation, seasize=args.seasize, mapprov=args.mapprov)
		with open(fn, 'w') as fh: json.dump(np.array(province_centers).tolist(), fh)

	fn = '{}/provmap.png'.format(args.o)
	if os.path.isfile(fn):
		if not args.quiet: print('Loading province areas...')
		with open(fn) as fh: province_areas = np.int64(Image.open(fn))
	else:
		print('Painting province areas...')
		province_areas = generate_province_areas(shape, wrap, elevation, province_centers)
		Image.fromarray(np.uint16(province_areas)).save(fn)

	fn = '{}/neighbors.json'.format(args.o)
	if os.path.isfile(fn):
		if not args.quiet: print('Loading neighbor table...')
		with open(fn) as fh: neighbors = [set(x) for x in json.load(fh)]
	else:
		print('Painting province areas...')
		neighbors = get_neighbors(province_areas)
		with open(fn, 'w') as fh: json.dump([np.array(list(x)).tolist() for x in neighbors], fh)

	print('Generating rivers...')
	rivers = generate_rivers(shape, wrap, elevation, province_centers, riverpart=args.riverpart)

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Another map generator for Dominions 5')
	parser.add_argument('-o', required=True, help='Output directory for data files')
	parser.add_argument('--riverpart', type=float, default=1.0)
	parser.add_argument('--bridges', type=float, default=0.6)
	#parser.add_argument('--extraislands', type=float, default=0.6)
	parser.add_argument('--seapart', type=float, default=0.3)
	parser.add_argument('--mountpart', type=float, default=0.2)
	parser.add_argument('--forestpart', type=float, default=0.2)
	parser.add_argument('--farmpart', type=float, default=0.15)
	parser.add_argument('--wastepart', type=float, default=0.1)
	parser.add_argument('--swamppart', type=float, default=0.1)
	parser.add_argument('--cavepart', type=float, default=0.03)
	parser.add_argument('--cavecluster', type=float, default=0.2)
	parser.add_argument('--kelppart', type=float, default=0.25)
	parser.add_argument('--gorgepart', type=float, default=0.25)
	parser.add_argument('--mapsize', type=int, nargs=2, default=[3000, 2000])
	parser.add_argument('--mapprov', type=int, default=150)
	parser.add_argument('--hills', type=float, default=1.5)
	parser.add_argument('--ruggedness', type=float, default=0.3)
	parser.add_argument('--seasize', type=float, default=3.5)
	parser.add_argument('--vwrap', action='store_true')
	parser.add_argument('--nohwrap', action='store_true')
	parser.add_argument('--quiet', action='store_true')
	parser.add_argument('--seed', type=int)

	args = parser.parse_args()
	if args.seed is None: seed = np.random.randint(2**31-1)
	else: seed = args.seed
	np.random.seed(seed)
	main(args)


'''
    --mapscol R G B A   Sea color 0-255 (default 54 54 130 255)
    --mapdscol R G B A  Deep Sea color 0-255 (default depending on mapscol)
    --mapccol R G B A   Coast color 0-255 (default depending on mapscol)
    --mapbcol R G B A   Ground border color 0-255
    --mapsbcol R G B A  Sea border color 0-255
    --mapbtopcol R G B A   Top border color 0-255
    --mapnoise X     Ground color noise 0-255 (default 15)
    --mapdirt X      Amount of dirt blobs (default 100)
    --mapdirtcol X   Color variance of dirt blobs (default 5)
    --mapdirtsize X  Size of dirt blobs (default 100)
    --borderwidth X  Border width 0-500 (default 100)
'''
'''
    --hills X        Number of Hills/Craters (default 150)
    --rugedness X    Rugedness 0-100 (default 30)
    --seasize X      Sea size, 100=normal land size (default 350)
    --mapnospr       Don't draw any sprites on the map
    --vwrap          Make map wrap north/south
    --nohwrap        Make map not wrap east/west
    --mapbunch X     When making a bunch, make this many maps (default 12)
    --quiet          Don't print stuff
'''

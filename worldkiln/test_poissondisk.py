#!/usr/bin/env python

import kilnfuel
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt


with Image.open('kiltest/elevation.png') as im: arr = np.float64(im) / (2**16-1)
np.random.seed(2)
density = np.ones(arr.shape)
#density[arr < 0.25] *= 3.5
density[arr < 0.375] /= 3.5 ** 0.5
density[arr < 0.25] /= 3.5
density[arr < 0.125] /= 3.5 ** 2
points = kilnfuel.noise.poisson_disk(arr.shape, density=density, targetnum=70, wrap=(1,1))
#points = kilnfuel.noise.poisson_disk(arr.shape, targetnum=100)

print(len(points))
plt.imshow(arr, cmap=kilnfuel.cmap.elevation, vmin=0, vmax=1)
plt.imshow(density)
plt.plot([p[1] for p in points], [p[0] for p in points], marker='.', color='r', lw=0)
plt.show()

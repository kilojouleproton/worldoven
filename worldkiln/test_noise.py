#!/usr/bin/env python
import kilnfuel.noise
import matplotlib.pyplot as plt

shape = 1500, 2000
wrap = 1, 1
whitenoise = kilnfuel.noise.white_noise(shape)
#plt.imshow(whitenoise)
#plt.show()

gaussfilt = kilnfuel.noise.low_pass_gaussian(whitenoise, radius=64)
##gaussfilt = kilnfuel.noise.low_pass_gaussian(whitenoise, maxfreq=200)
plt.imshow(gaussfilt)
plt.show()

#ungaussfilt = kilnfuel.noise.high_pass_gaussian(gaussfilt, radius=4)
#plt.imshow(ungaussfilt)
#plt.show()

#bandfilt = kilnfuel.noise.normalize(kilnfuel.noise.band_pass_gaussian(whitenoise, 32, 36))
#plt.imshow(bandfilt)
#plt.show()

whitenoise = kilnfuel.noise.white_noise([shape[0]*2, shape[1]*2])
gaussfilt = kilnfuel.noise.low_pass_gaussian(whitenoise, radius=64)
gaussfilt = gaussfilt[:shape[0],:shape[1]]
plt.imshow(gaussfilt)
plt.show()

angplify = kilnfuel.noise.amplify(gaussfilt, maxfreq=1, factor=16)
plt.imshow(angplify)
plt.show()

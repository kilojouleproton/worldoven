#!/usr/bin/env python

import matplotlib.pyplot as plt
import numpy as np

def in_image(somepoint, shape):
	if (somepoint < 0).any(): return False
	elif (somepoint[0] >= shape[0]): return False
	elif (somepoint[1] >= shape[1]): return False
	else: return True

def no_neighbor(grid, somepoint, mindist, cellsize, outlist, wrap, v):
	center = tuple(np.int64(somepoint//cellsize[v]))
	sqrad = mindist[v]**2

	for r in range(-2, 2+1):
		adjr = center[0] + r
		if wrap[0]: adjr = adjr % len(grid[v])
		if not (0 <= adjr < len(grid[v])): continue

		for c in range(-2, 2+1):
			adjc = center[1] + c
			if wrap[1]: adjc = adjc % len(grid[v][0])
			if not (0 <= adjc < len(grid[v][0])): continue

			if not grid[v][adjr][adjc]: continue
			else:
				for oldindex in grid[v][adjr][adjc]:
					oldpoint = outlist[oldindex]
					vec = somepoint - oldpoint
					if (vec[1]**2 + vec[0]**2) < sqrad: return False
	return True

def throw_dart(point, mindist):
	rad = np.random.random() * mindist + mindist
	theta = np.random.random() * 2 * np.pi

	return point + np.array((rad*np.cos(theta), rad*np.sin(theta)))

def Grid(shape):
	grid = []
	for r in range(shape[0]):
		grid.append([])
		for c in range(shape[1]):
			grid[r].append(set())
	return grid

def place_n(shape, relsizemap, attempts=30, npoints=150, wrap=None):
	#relsize: areawise
	#mindist given intended points: area/360000*npoints**-0.537
	area = shape[0] * shape[1]

	if wrap is None: wrap = (False, False)

	meansize = np.mean(relsizemap)
	counts = {}
	for v in np.ravel(relsizemap): 
		if v not in counts: counts[v] = 0
		counts[v] += 1

	harmsqsum = 0
	for v in counts: harmsqsum += counts[v]/v**2
	mindist = {}
	magicnumber = 1.8
	for v in counts: 
		#mindist[v] = counts[v]/360000 * (npoints * counts[v]/v**2 / harmsqsum)**-0.537
		#=(A35*B35/1000000)^0.537*(1/360000*D35)^(1/-1.86)
		contribution = npoints * counts[v]/v**2 / harmsqsum
		mindist[v] = (counts[v]/1000000)**0.537 * (1/360000*contribution)**-0.537 * magicnumber

	cellsize = {}
	grid = {}
	for v in mindist:
		cellsize[v] = mindist[v]/2**.5
		gridshape = int(np.ceil(shape[0]/cellsize[v])), int(np.ceil(shape[1]/cellsize[v]))
		#grid[v] = -np.ones(gridshape, dtype=np.int64)
		grid[v] = Grid(gridshape)

	initpoint = np.array((np.random.random() * (shape[0]-1), np.random.random() * (shape[1]-1)))

	proclist = []
	proclist.append(initpoint)
	outlist = []
	outlist.append(initpoint)
	pointrelsize = []
	pointrelsize.append(relsizemap[tuple(np.int64(initpoint))])

	for v in cellsize: 
		r, c = np.int64(initpoint//cellsize[v])
		grid[v][r][c].add(0)

	while proclist:
		index = np.random.randint(len(proclist))
		point = proclist.pop(index)
		relsize = pointrelsize.pop(index)

		for i in range(attempts):
			
			newpoint = throw_dart(point, mindist[relsize])
			if wrap[0]: newpoint[0] = newpoint[0] % shape[0]
			else: newpoint[0] = np.clip(newpoint[0], 0, shape[0]-1)
			if wrap[1]: newpoint[1] = newpoint[1] % shape[1]
			else: newpoint[1] = np.clip(newpoint[1], 0, shape[1]-1)

			within_bounds = in_image(newpoint, shape)
			far_enough = no_neighbor(grid, newpoint, mindist, cellsize, outlist, wrap, relsize)

			if within_bounds and far_enough:
				proclist.append(newpoint)
				outlist.append(newpoint)
				pointrelsize.append(relsizemap[tuple(np.int64(newpoint))])

				#for v in grid: grid[v][tuple(np.int64(newpoint//cellsize[v]))] = len(outlist) - 1
				for v in grid: 
					r, c = np.int64(newpoint//cellsize[v])
					grid[v][r][c].add(len(outlist) - 1)

	return outlist

def place_many(shape, relsizemap, attempts=30, base_mindist=100, wrap=None, initpoints=None):
	print('placing many things')
	relsizemap = np.clip(relsizemap, 0.01, 1)

	#maxsep = min(base_mindist / np.min(relsizemap), base_mindist / np.mean(relsizemap) * 2)
	maxsep = base_mindist / np.min(relsizemap)

	cellsize = maxsep/2**.5
	gridshape = int(np.ceil(shape[0]/cellsize)), int(np.ceil(shape[1]/cellsize))
	#grid = -np.ones(gridshape, dtype=np.int64)
	grid = Grid(gridshape)

	proclist = []
	outlist = []
	pointrelsize = []


	initpoint = np.array((np.random.random() * (shape[0]-1),  np.random.random() * (shape[1]-1)))
	proclist.append(initpoint)
	outlist.append(initpoint)
	pointrelsize.append(base_mindist/relsizemap[tuple(np.int64(initpoint))])
	grid[int(initpoint[0]/cellsize)][int(initpoint[1]/cellsize)].add(len(outlist)-1)

	if initpoints is not None:
		for initpoint in initpoints:
			proclist.append(initpoint)
			outlist.append(initpoint)
			pointrelsize.append(base_mindist/relsizemap[tuple(np.int64(initpoint))])
			grid[int(initpoint[0]/cellsize)][int(initpoint[1]/cellsize)].add(len(outlist)-1)
					

	while proclist:	
		if len(outlist) > 500: break
		index = np.random.randint(len(proclist))
		point = proclist.pop(index)
		relsize = pointrelsize.pop(index)

		for i in range(attempts):
			newpoint = throw_dart(point, relsize)
			if wrap[0]: newpoint[0] = newpoint[0] % shape[0]
			else: newpoint[0] = np.clip(newpoint[0], 0, shape[0]-1)
			if wrap[1]: newpoint[1] = newpoint[1] % shape[1]
			else: newpoint[1] = np.clip(newpoint[1], 0, shape[1]-1)

			newrelsize = base_mindist/relsizemap[tuple(np.int64(newpoint))]

			within_bounds = in_image(newpoint, shape)
			far_enough = no_neighbor([grid], newpoint, [newrelsize], [cellsize], outlist, wrap, 0)

			if within_bounds and far_enough:
				proclist.append(newpoint)
				outlist.append(newpoint)
				pointrelsize.append(base_mindist/relsizemap[tuple(np.int64(newpoint))])
				grid[int(newpoint[0]/cellsize)][int(newpoint[1]/cellsize)].add(len(outlist)-1)

	y = [p[0] for p in outlist]
	x = [p[1] for p in outlist]

	return outlist
def test_bluenoise():
	shape = 2000, 1000
	relsizemap = np.ones(shape)
	relsizemap[:,:shape[1]//2] *= (3.5**.5)

	points = place_n(shape, relsizemap, npoints=400, wrap=(1,0))
	print(len(points))

	fig = plt.figure()
	ax = fig.add_subplot(111)
	from scipy.spatial import Voronoi, voronoi_plot_2d
	vor = Voronoi(points)
	voronoi_plot_2d(vor, ax=ax)
	plt.show()

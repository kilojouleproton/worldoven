#!/usr/bin/env python

import matplotlib.pyplot as plt
import subdivision
import numpy as np

import unittest

SILENT = False

class TestEdges(unittest.TestCase):
	startpt = np.array([0,0])
	endpt = np.array([1,3])

	def test_edgecompleteness(self):
		edge = subdivision.Edge()
		self.assertRaises(subdivision.IncompleteEdgeError, edge.__truediv__, 1)

	def test_edgezerodiv(self):
		edge = subdivision.Edge(self.startpt, self.endpt)
		newedge = edge / 0
		assert edge == newedge

		newedge == edge // 1
		assert edge == newedge

		self.assertRaises(ZeroDivisionError, edge.__floordiv__, 0)

	def test_subdivide(self):
		edge = subdivision.Edge(self.startpt, self.endpt)
		
		newedges = edge.displace_midpoint(0.1)

	def test_plot(self):
		if SILENT: return
		ax = plt.figure().gca()

		edge = subdivision.Edge(self.startpt, self.endpt)
		edge.draw(ax)

		for i in [0.1, 0.2, 0.3, 0.4, 0.5, -0.1, -0.2, -0.3, -0.4, -0.5]:
			newedges = edge.displace_midpoint(i)
			[e.draw(ax) for e in newedges]

		ax.set_xlim([0, 6])
		ax.set_ylim([0, 6])
		plt.show()

class TestTriangle(unittest.TestCase):
	vertices = np.array([[1,1], [3,1], [2,2]])

	def test_plot(self):
		if SILENT: return
		ax = plt.figure().gca()

		polygon = subdivision.Polygon(vertices=self.vertices)
		polygon.fill(ax, color='orange')
		polygon.draw(ax)

		ax.set_xlim([0, 6])
		ax.set_ylim([0, 6])
		plt.show()

	def test_concavity(self):
		polygon = subdivision.Polygon(vertices=self.vertices)
		assert polygon.is_convex()

class TestPolygon(unittest.TestCase):
	convex = subdivision.Polygon(vertices=np.array([[1,1], [3,1], [4,3], [2,4], [0,3]]))
	concave = subdivision.Polygon(vertices=np.array([[1,1], [3,1], [4,3], [2,2], [0,3]]))
	selfintersecting = subdivision.Polygon(vertices=np.array([[1,1], [4,3], [3,1], [0,3]]))

	def test_draw(self):
		ax = plt.figure().gca()
		self.convex.fill(ax, alpha=0.5, color='tab:blue')
		self.concave.fill(ax, alpha=0.5, color='tab:orange')
		self.selfintersecting.fill(ax, alpha=0.5, color='tab:green')

		ax.set_xlim([0, 6])
		ax.set_ylim([0, 6])
		plt.show()

	def test_concavity(self):
		assert self.convex.is_convex()
		assert not self.concave.is_convex()

	#def test_concavity_selfintersecting(self):
	#	assert not self.selfintersecting.is_convex()

	def test_area(self):
		assert abs(self.convex.get_area() - 8) < 1e-9 #close enough. stupid floating point errors
		assert abs(self.concave.get_area() - 4) < 1e-9

if __name__ == '__main__':
	unittest.main()

